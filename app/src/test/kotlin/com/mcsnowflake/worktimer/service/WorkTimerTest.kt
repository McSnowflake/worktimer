/*
package com.mcsnowflake.worktimer.service

import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.mcsnowflake.worktimer.AUTO_SWITCH_MODE
import com.mcsnowflake.worktimer.OVERTIME_DURATION
import com.mcsnowflake.worktimer.SHORT_BREAK_REPETITIONS
import com.mcsnowflake.worktimer.misc.getDuration
import com.mcsnowflake.worktimer.misc.getDurationFor
import com.mcsnowflake.worktimer.misc.getOrDefault
import com.mcsnowflake.worktimer.service.TimerState.Running.Finished
import com.mcsnowflake.worktimer.service.TimerState.Running.Progress
import com.mcsnowflake.worktimer.service.TimerState.Running.Started
import com.mcsnowflake.worktimer.service.TimerState.Running.Terminated
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.LONG_BREAK
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.SHORT_BREAK
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.core.test.TestResult
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import java.time.Duration
import java.time.LocalDateTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain

@ExperimentalCoroutinesApi
class WorkTimerTest : StringSpec() {

    private val testDispatcher = TestCoroutineDispatcher() // the dispatcher

    override suspend fun beforeTest(testCase: TestCase) {
        super.beforeTest(testCase)
        mockkStatic(Log::class)
        every { Log.v(any(), any()) } returns 0
        every { Log.d(any(), any()) } returns 0
        every { Log.i(any(), any()) } returns 0
        every { Log.e(any(), any()) } returns 0
        Dispatchers.setMain(testDispatcher) // set the coroutine context
    }

    override suspend fun afterTest(testCase: TestCase, result: TestResult) {
        super.afterTest(testCase, result)
        Dispatchers.resetMain() // reset
        testDispatcher.cleanupTestCoroutines() // clear all
        clearAllMocks()
    }

    init {
        "Timer starts in state working and switches correctly to long and short breaks and back in auto switch mode" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testPreferences = mockk<DataStore<Preferences>>()
                mockkStatic("com.mcsnowflake.worktimer.misc.PreferencesHelperKt")
                coEvery { testPreferences.getDurationFor(any()) } returns Duration.ofSeconds(1)
                coEvery { testPreferences.getOrDefault(AUTO_SWITCH_MODE) } returns true
                coEvery { testPreferences.getOrDefault(SHORT_BREAK_REPETITIONS) } returns 3
                val testObject = WorkTimer(testScope, testPreferences)

                val testStart = LocalDateTime.now()
                val results = mutableListOf<TimerState>()

                withConstantNow(testStart) {
                    val resultCollection = this.launch { testObject.events.collect { results.add(it) } }

                    runBlocking {
                        testObject.execute(TimerService.Command.START)
                        repeat(12) { testObject.execute(TimerService.Command.NEXT) }
                        testObject.execute(TimerService.Command.STOP)
                    }
                    resultCollection.cancel()

                    results shouldBe listOf(
                        Started(WORK_SESSION), Progress(WORK_SESSION, 0.0F, Duration.ofSeconds(1)), Finished(WORK_SESSION),
                        Started(sessionType = SHORT_BREAK), Finished(sessionType = SHORT_BREAK),
                        Started(sessionType = WORK_SESSION), Finished(sessionType = WORK_SESSION),
                        Started(sessionType = SHORT_BREAK), Finished(sessionType = SHORT_BREAK),
                        Started(sessionType = WORK_SESSION), Finished(sessionType = WORK_SESSION),
                        Started(sessionType = SHORT_BREAK), Finished(sessionType = SHORT_BREAK),
                        Started(sessionType = WORK_SESSION), Finished(sessionType = WORK_SESSION),
                        Started(sessionType = LONG_BREAK), Finished(sessionType = LONG_BREAK),
                        Started(sessionType = WORK_SESSION), Finished(sessionType = WORK_SESSION),
                        Started(sessionType = SHORT_BREAK), Finished(sessionType = SHORT_BREAK),
                        Started(sessionType = WORK_SESSION), Finished(sessionType = WORK_SESSION),
                        Started(sessionType = SHORT_BREAK), Finished(sessionType = SHORT_BREAK),
                        Started(sessionType = WORK_SESSION), Terminated
                    )
                }

                testScope.cancel()
            }
        }

        "Timer updates progress every second" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)

                val testPreferences = mockk<DataStore<Preferences>>()
                mockkStatic("com.mcsnowflake.worktimer.misc.PreferencesHelperKt")
                coEvery { testPreferences.getDurationFor(any()) } returns Duration.ofSeconds(10)
                coEvery { testPreferences.getOrDefault(AUTO_SWITCH_MODE) } returns true
                coEvery { testPreferences.getOrDefault(SHORT_BREAK_REPETITIONS) } returns 3
                val testObject = WorkTimer(testScope, testPreferences)

                val time = LocalDateTime.now()
                val results = mutableListOf<TimerState>()

                withConstantNow(time) {
                    val resultCollection = this.launch { testObject.events.collect { results.add(it) } }

                    runBlocking {
                        testObject.execute(TimerService.Command.START)
                        repeat(9) {
                            withConstantNow(time.plusSeconds(it.toLong())) {
                                advanceTimeBy(it * 100L)
                            }
                        }
                        testObject.execute(TimerService.Command.STOP)
                        resultCollection.cancel()
                    }

                    results shouldBe listOf(
                        Started(sessionType = WORK_SESSION),
                        Progress(sessionType = WORK_SESSION, progress = 0.0F, remainingTime = Duration.ofSeconds(10)),
                        Progress(sessionType = WORK_SESSION, progress = 0.1F, remainingTime = Duration.ofSeconds(9)),
                        Progress(sessionType = WORK_SESSION, progress = 0.2F, remainingTime = Duration.ofSeconds(8)),
                        Progress(sessionType = WORK_SESSION, progress = 0.3F, remainingTime = Duration.ofSeconds(7)),
                        Progress(sessionType = WORK_SESSION, progress = 0.4F, remainingTime = Duration.ofSeconds(6)),
                        Progress(sessionType = WORK_SESSION, progress = 0.5F, remainingTime = Duration.ofSeconds(5)),
                        Progress(sessionType = WORK_SESSION, progress = 0.6F, remainingTime = Duration.ofSeconds(4)),
                        Progress(sessionType = WORK_SESSION, progress = 0.7F, remainingTime = Duration.ofSeconds(3)),
                        Progress(sessionType = WORK_SESSION, progress = 0.8F, remainingTime = Duration.ofSeconds(2)),
                        Terminated
                    )
                }
                testScope.cancel()
            }
        }

        "Timer finishes countdown" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testPreferences = mockk<DataStore<Preferences>>()
                mockkStatic("com.mcsnowflake.worktimer.misc.PreferencesHelperKt")
                coEvery { testPreferences.getDurationFor(any()) } returns Duration.ofSeconds(2)
                coEvery { testPreferences.getOrDefault(AUTO_SWITCH_MODE) } returns false
                coEvery { testPreferences.getOrDefault(SHORT_BREAK_REPETITIONS) } returns 3
                val testObject = WorkTimer(testScope, testPreferences)

                val time = LocalDateTime.now()
                val results = mutableListOf<TimerState>()

                withConstantNow(time) {
                    val resultCollection = this.launch { testObject.events.collect { results.add(it) } }

                    runBlocking {
                        testObject.execute(TimerService.Command.START)
                        repeat(4) {
                            withConstantNow(time.plusSeconds(it.toLong())) {
                                advanceTimeBy(it * 100L)
                            }
                        }
                        testObject.execute(TimerService.Command.STOP)
                    }

                    resultCollection.cancel()

                    results shouldBe listOf(
                        Started(sessionType = WORK_SESSION),
                        Progress(sessionType = WORK_SESSION, progress = 0.0F, remainingTime = Duration.ofSeconds(2)),
                        Progress(sessionType = WORK_SESSION, progress = 0.5F, remainingTime = Duration.ofSeconds(1)),
                        Finished(sessionType = WORK_SESSION),
                        Terminated
                    )
                }
                testScope.cancel()
            }
        }

        "Timer prolongs current session upon more command for the configured overTime" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testPreferences = mockk<DataStore<Preferences>>()
                mockkStatic("com.mcsnowflake.worktimer.misc.PreferencesHelperKt")
                coEvery { testPreferences.getDurationFor(any()) } returns Duration.ofSeconds(4)
                coEvery { testPreferences.getOrDefault(AUTO_SWITCH_MODE) } returns false
                coEvery { testPreferences.getDuration(OVERTIME_DURATION) } returns Duration.ofSeconds(2)
                coEvery { testPreferences.getOrDefault(SHORT_BREAK_REPETITIONS) } returns 3
                val testObject = WorkTimer(testScope, testPreferences)

                val time = LocalDateTime.now()
                val results = mutableListOf<TimerState>()

                withConstantNow(time) {
                    val resultCollection = this.launch { testObject.events.collect { results.add(it) } }

                    runBlocking {
                        testObject.execute(TimerService.Command.START)
                        repeat(4) {
                            withConstantNow(time.plusSeconds(it.toLong())) {
                                advanceTimeBy(it * 100L)
                                if (it == 2) testObject.execute(TimerService.Command.MORE)
                            }
                        }
                        testObject.execute(TimerService.Command.STOP)
                    }

                    resultCollection.cancel()

                    println(results)

                    results shouldBe listOf(
                        Started(sessionType = WORK_SESSION),
                        Progress(sessionType = WORK_SESSION, progress = 0.0F, remainingTime = Duration.ofSeconds(4)),
                        Progress(sessionType = WORK_SESSION, progress = 0.25F, remainingTime = Duration.ofSeconds(3)),
                        Progress(sessionType = WORK_SESSION, progress = 0.5F, remainingTime = Duration.ofSeconds(2)),
                        Progress(sessionType = WORK_SESSION, progress = 0.33333334F, remainingTime = Duration.ofSeconds(4)),
                        Progress(sessionType = WORK_SESSION, progress = 0.5F, remainingTime = Duration.ofSeconds(3)),
                        Terminated
                    )
                }
                testScope.cancel()
            }
        }

        "Timer restarts upon more command for the configured overTime if session has finished" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testPreferences = mockk<DataStore<Preferences>>()
                mockkStatic("com.mcsnowflake.worktimer.misc.PreferencesHelperKt")
                coEvery { testPreferences.getDurationFor(any()) } returns Duration.ofSeconds(2)
                coEvery { testPreferences.getOrDefault(AUTO_SWITCH_MODE) } returns false
                coEvery { testPreferences.getDuration(OVERTIME_DURATION) } returns Duration.ofSeconds(2)
                coEvery { testPreferences.getOrDefault(SHORT_BREAK_REPETITIONS) } returns 3
                val testObject = WorkTimer(testScope, testPreferences)

                val time = LocalDateTime.now()
                val results = mutableListOf<TimerState>()

                withConstantNow(time) {
                    val resultCollection = this.launch { testObject.events.collect { results.add(it) } }

                    runBlocking {
                        testObject.execute(TimerService.Command.START)
                        repeat(4) {
                            withConstantNow(time.plusSeconds(it.toLong())) {
                                advanceTimeBy(it * 100L)
                                if (it == 3) testObject.execute(TimerService.Command.MORE)
                            }
                        }
                        testObject.execute(TimerService.Command.STOP)
                    }

                    resultCollection.cancel()

                    results shouldBe listOf(
                        Started(sessionType = WORK_SESSION),
                        Progress(sessionType = WORK_SESSION, progress = 0.0F, remainingTime = Duration.ofSeconds(2)),
                        Progress(sessionType = WORK_SESSION, progress = 0.5F, remainingTime = Duration.ofSeconds(1)),
                        Finished(sessionType = WORK_SESSION), Started(sessionType = WORK_SESSION),
                        Progress(sessionType = WORK_SESSION, progress = 0.6F, remainingTime = Duration.ofSeconds(2)),
                        Terminated
                    )
                }
                testScope.cancel()
            }
        }
    }
}
*/
