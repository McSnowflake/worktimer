package com.mcsnowflake.worktimer.service

import android.app.Service
import android.util.Log
import com.mcsnowflake.worktimer.notifications.NotificationHandler
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.core.test.TestResult
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.koin.core.context.startKoin
import org.koin.dsl.module

@ExperimentalCoroutinesApi
class TimerServiceIntentTests : StringSpec() {

    private val testDispatcher = TestCoroutineDispatcher() // the dispatcher

    override suspend fun beforeTest(testCase: TestCase) {
        super.beforeTest(testCase)
        mockkStatic(Log::class)
        every { Log.v(any(), any()) } returns 0
        every { Log.d(any(), any()) } returns 0
        every { Log.i(any(), any()) } returns 0
        every { Log.e(any(), any()) } returns 0

        mockkStatic(Service::class)

        Dispatchers.setMain(testDispatcher) // set the coroutine context
        startKoin { modules(appModule) }
    }

    override suspend fun afterTest(testCase: TestCase, result: TestResult) {
        super.afterTest(testCase, result)
        Dispatchers.resetMain() // reset
        testDispatcher.cleanupTestCoroutines() // clear all
    }

    private var notFactory = mockk<NotificationHandler>()
    private var testTimer = mockk<WorkTimer>()

    private val appModule = module {
        single { testTimer }
        single { notFactory }
    }

    //  override fun listeners() = listOf(KoinListener(appModule))

    init {

        //  withData(TimerService.Command.values().toList()) { assertCommandPassThrough(it) }

//        "START intent should configure and start timer" {
//            testDispatcher.runBlockingTest {
//
//                val testIntent = mockk<Intent>()
//                every { testIntent.action } returns TimerService.Command.START.name
//
//                testTimer = mockk()
//                coEvery { testTimer.execute(TimerService.Command.START) } just Runs
//
//                val sut = TimerService(this)
//
//                sut.onStartCommand(testIntent, 0, 0)
//
//                coVerify(exactly = 1) { testTimer.execute(TimerService.Command.START) }
//            }
//        }
//
//       "NEXT intent should stop, toggle state of and restart timer" {
//            testDispatcher.runBlockingTest {
//
//                val testIntent = mockk<Intent>()
//                every { testIntent.action } returns TimerService.Command.NEXT.name
//
//                testTimer = mockk()
//                coEvery { testTimer.execute(TimerService.Command.NEXT) } just Runs
//
//                val sut = TimerService(this)
//
//                sut.onStartCommand(testIntent, 0, 0)
//
//                coVerify(exactly = 1) { testTimer.execute(TimerService.Command.NEXT) }
//            }
//        }
/*
        "MORE intent should prolong timer" {
            testDispatcher.runBlockingTest {

                val testIntent = mockk<Intent>()
                every { testIntent.action } returns TimerService.Action.MORE.name

                testTimer = mockk()
                coEvery { testTimer.moreTime() } just Runs

                val sut = TimerService(this)

                sut.onStartCommand(testIntent, 0, 0)

                coVerify(exactly = 1) { testTimer.moreTime() }
            }
        }

        "STOP intent should stop service" {
            testDispatcher.runBlockingTest {

                val testIntent = mockk<Intent>()
                every { testIntent.action } returns TimerService.Action.STOP.name
                testTimer = mockk()
                coEvery { testTimer.stop() } just Runs

                val sut = spyk(TimerService(this))

                sut.onStartCommand(testIntent, 0, 0)

                verify(exactly = 1) { sut.stopSelf() }
                coVerify(exactly = 1) { testTimer.stop() }
            }
        }*/
    }

    // private fun assertCommandPassThrough(command: TimerService.Command) = testDispatcher.runBlockingTest {
    //
    //     val testIntent = mockk<Intent>()
    //     every { testIntent.action } returns command.name
    //     testTimer = mockk(relaxed = true)
    //     TimerService(this).onStartCommand(testIntent, 0, 0)
    //     coVerify(exactly = 1) { testTimer.execute(command) }
    // }
}
