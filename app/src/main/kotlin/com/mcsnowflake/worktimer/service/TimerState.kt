package com.mcsnowflake.worktimer.service

import java.time.Duration

sealed interface TimerState {

    sealed interface Running : TimerState {
        object Created : Running {
            override fun toString(): String = javaClass.simpleName
        }
        data class Prolonged(
            override val sessionType: WorkTimer.SessionType,
            val remainingTime: Duration
        ) : Running, WithSession

        data class Started(
            override val sessionType: WorkTimer.SessionType,
            val remainingTime: Duration
        ) : Running, WithSession

        data class Progress(
            override val sessionType: WorkTimer.SessionType,
            val progress: Float,
            val remainingTime: Duration
        ) : Running, WithSession

        data class Finished(override val sessionType: WorkTimer.SessionType) : Running, WithSession
        object Terminated : Running {
            override fun toString(): String = javaClass.simpleName
        }
    }

    object NotRunning : TimerState {
        override fun toString(): String = javaClass.simpleName
    }
}

interface WithSession {
    val sessionType: WorkTimer.SessionType

    fun isWorkSession() = sessionType == WorkTimer.SessionType.WORK_SESSION
}
