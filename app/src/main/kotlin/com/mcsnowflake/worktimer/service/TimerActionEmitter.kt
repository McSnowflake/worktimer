package com.mcsnowflake.worktimer.service

import android.content.Context

class TimerActionEmitter(private val context: Context) {
    fun emit(command: TimerService.Command) {
        val intent = TimerService.getIntent(context, command)
        context.startForegroundService(intent)
    }
}
