package com.mcsnowflake.worktimer.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.mcsnowflake.worktimer.DND_MODE
import com.mcsnowflake.worktimer.misc.getAsFlow
import com.mcsnowflake.worktimer.notifications.DoNotDisturbModeHandler
import com.mcsnowflake.worktimer.notifications.NotificationHandler
import com.mcsnowflake.worktimer.notifications.NotificationHandler.Companion.TimerNotificationID
import com.mcsnowflake.worktimer.service.TimerState.Running.Terminated
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class TimerService(
    private val serviceScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
) : Service() {

    // a service must have a zero argument constructor, so we have to inject the dependencies
    private val timer by inject<WorkTimer> { parametersOf(serviceScope) }
    private val notificationFactory by inject<NotificationHandler>()
    private val doNotDisturbModeHandler by inject<DoNotDisturbModeHandler>()
    private val preferences by inject<DataStore<Preferences>>()

    override fun onCreate() {
        super.onCreate()

        // make service a foreground service
        startForeground(TimerNotificationID, notificationFactory.getStartNotification())

        // setup event processing
        timer.events.onEach {
            Log.d("TimerEvent", it.toString())
            notificationFactory.process(it)
            //   if (isDndEnabled()) doNotDisturbModeHandler.process(it)
            if (it is Terminated) stopSelf()
        }.combine(preferences.getAsFlow(DND_MODE)) { event, isDndEnabled -> if (isDndEnabled) doNotDisturbModeHandler.process(event) }
            .launchIn(serviceScope)

        Log.d(javaClass.simpleName, "TimerService started $serviceScope")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(javaClass.simpleName, "Intend received: ${intent.action}")
        serviceScope.launch { kotlin.runCatching { timer.execute(intent.getCommand()) } }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        serviceScope.cancel()
        Log.d(javaClass.simpleName, "TimerService stopped")
        super.onDestroy()
    }

    inner class LocalBinder : Binder() {
        fun getTimerEvents() = timer.events
    }

    override fun onBind(intent: Intent): IBinder {
        return LocalBinder()
    }

    companion object {
        fun getIntent(applicationContext: Context, command: Command) = getIntent(applicationContext).apply { this.action = command.name }
        fun getIntent(applicationContext: Context) = Intent(applicationContext, TimerService::class.java)
    }

    enum class Command {
        START, MORE, NEXT, STOP;
    }

    private fun Intent.getCommand() = Command.valueOf(action!!)
}
