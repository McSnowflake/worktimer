package com.mcsnowflake.worktimer.service

import com.mcsnowflake.worktimer.R
import com.mcsnowflake.worktimer.service.TimerService.Command

enum class TimerAction(val serviceCommand: Command) {
    PAUSE(Command.NEXT),
    WORK(Command.NEXT),
    RESUME(Command.NEXT),
    STOP(Command.STOP),
    MORE(Command.MORE),
    START(Command.NEXT);

    fun getActionTitle() = when (this) {
        PAUSE -> "Give me a break"
        WORK -> "Onto it!"
        RESUME -> "OK"
        STOP -> "Let's call it a day"
        MORE -> "Just one moment"
        START -> "Start"
    }

    fun getActionIcon() = when (this) {
        PAUSE -> R.drawable.baseline_check_circle_outline_24
        WORK -> R.drawable.baseline_check_circle_outline_24
        RESUME -> R.drawable.baseline_highlight_off_24
        STOP -> R.drawable.baseline_history_24
        MORE -> R.drawable.baseline_history_24
        START -> R.drawable.baseline_check_circle_outline_24
    }
}
