package com.mcsnowflake.worktimer.service

import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.mcsnowflake.worktimer.AUTO_SWITCH_MODE
import com.mcsnowflake.worktimer.OVERTIME_DURATION
import com.mcsnowflake.worktimer.SHORT_BREAK_REPETITIONS
import com.mcsnowflake.worktimer.misc.getDuration
import com.mcsnowflake.worktimer.misc.getDurationFor
import com.mcsnowflake.worktimer.misc.getOrDefault
import com.mcsnowflake.worktimer.service.TimerService.Command
import com.mcsnowflake.worktimer.service.TimerState.Running.Finished
import com.mcsnowflake.worktimer.service.TimerState.Running.Progress
import com.mcsnowflake.worktimer.service.TimerState.Running.Prolonged
import com.mcsnowflake.worktimer.service.TimerState.Running.Started
import com.mcsnowflake.worktimer.service.TimerState.Running.Terminated
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.LONG_BREAK
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.SHORT_BREAK
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION
import java.time.Duration.between
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import kotlin.math.ceil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.withContext

class WorkTimer(
    private val scope: CoroutineScope,
    private val preferences: DataStore<Preferences>
) {
    private val _events = MutableSharedFlow<TimerState.Running>(1, 2, DROP_OLDEST)
    val events = _events.asSharedFlow()
    private val currentSession = MutableStateFlow(WORK_SESSION)

    private lateinit var timerLoop: Job
    private val startTime = MutableStateFlow<LocalDateTime>(now())
    private val endTime = MutableStateFlow<LocalDateTime>(now())

    private var currentRepetition = 0

    suspend fun execute(command: Command) = withContext(Dispatchers.IO) {
        when (command) {
            Command.START -> startTimer()
            Command.NEXT -> nextState()
            Command.MORE -> moreTime()
            Command.STOP -> stopTimer()
        }
    }

    private suspend fun startTimer() {
        resetTime()
        startTimerLoop()
        print("starting")
    }

    private suspend fun moreTime() {
        if (isTimerLoopRunning()) {
            endTime.value = endTime.value.plus(preferences.getDuration(OVERTIME_DURATION))
            _events.emit(Prolonged(currentSession.value, between(now(), endTime.value)))
            emitProgressUpdate()
        } else {
            endTime.value = now().plus(preferences.getDuration(OVERTIME_DURATION))
            startTimerLoop()
        }
    }

    private suspend fun nextState() {
        if (isTimerLoopRunning()) _events.emit(Finished(currentSession.value))
        setNextState()
        resetTime()
        if (isTimerLoopRunning()) _events.emit(Started(currentSession.value, between(now(), endTime.value)))
        else startTimerLoop()
    }

    private fun stopTimer() {
        if (isTimerLoopRunning()) timerLoop.cancel()
        _events.tryEmit(Terminated)
    }

    private suspend fun setNextState() {
        currentSession.value = when (currentSession.value) {
            WORK_SESSION ->
                if (currentRepetition >= preferences.getOrDefault(SHORT_BREAK_REPETITIONS)) {
                    currentRepetition = 0
                    LONG_BREAK
                } else {
                    currentRepetition++
                    SHORT_BREAK
                }
            SHORT_BREAK -> WORK_SESSION
            LONG_BREAK -> WORK_SESSION
        }
    }

    private fun startTimerLoop() {
        timerLoop = flow {
            _events.emit(Started(currentSession.value, between(now(), endTime.value)))
            // Log.(javaClass.simpleName, "count down started")
            while (now().isBefore(endTime.value) || preferences.getOrDefault(AUTO_SWITCH_MODE)) {
                if (now().isAfter(endTime.value)) nextState()
                emit(ceil(between(startTime.value, now()).toMillis().toFloat().div(1000)).toInt())
                delay(100)
            }
            // Log.d(javaClass.simpleName, "count down ended")
            _events.emit(Finished(currentSession.value))
        }
            .distinctUntilChanged()
            .onEach { emitProgressUpdate() }
            .launchIn(scope)
    }

    private suspend fun emitProgressUpdate() {
        val progressTime = between(startTime.value, now()).toMillis()
        val totalTime = between(startTime.value, endTime.value).toMillis()
        val progress = progressTime.toFloat().div(totalTime)

        val remainingTime = between(now(), endTime.value)

        _events.emit(Progress(currentSession.value, progress, remainingTime))
    }

    private suspend fun resetTime() {
        startTime.value = now()
        endTime.value = startTime.value.plus(preferences.getDurationFor(currentSession.value))
        Log.d(javaClass.simpleName, "timer reset to start date: " + startTime + "and end date: " + endTime)
    }

    private fun isTimerLoopRunning() = ::timerLoop.isInitialized && timerLoop.isActive

    enum class SessionType {
        WORK_SESSION,
        SHORT_BREAK,
        LONG_BREAK;
    }
}
