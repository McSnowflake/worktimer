package com.mcsnowflake.worktimer

import android.app.AlarmManager
import android.app.NotificationManager
import android.content.Context
import android.media.AudioManager
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.mcsnowflake.worktimer.notifications.DoNotDisturbModeHandler
import com.mcsnowflake.worktimer.notifications.NotificationHandler
import com.mcsnowflake.worktimer.service.TimerActionEmitter
import com.mcsnowflake.worktimer.service.WorkTimer
import com.mcsnowflake.worktimer.ui.components.appbar.AppBarViewModel
import com.mcsnowflake.worktimer.ui.views.dashboard.DashBoardViewModel
import com.mcsnowflake.worktimer.ui.views.settings.SettingsViewModel
import com.mcsnowflake.worktimer.ui.views.setupView.SetupViewModel
import kotlinx.coroutines.CoroutineScope
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun getModule(dataStore: DataStore<Preferences>) = module {

    single { dataStore }
    factory { (scope: CoroutineScope) -> WorkTimer(scope, get()) }

    single { androidContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }
    single { androidContext().getSystemService(Context.AUDIO_SERVICE) as AudioManager }
    single { androidContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager }

    single { NotificationHandler(androidContext(), get(), get(), get()) }
    single { DoNotDisturbModeHandler(get()) }
    single { TimerActionEmitter(androidContext()) }

    viewModel { DashBoardViewModel(get()) }
    viewModel { SettingsViewModel(get()) }
    viewModel { SetupViewModel(get()) }
    viewModel { AppBarViewModel() }
}
