package com.mcsnowflake.worktimer.misc

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.mcsnowflake.worktimer.LONG_BREAK_DURATION
import com.mcsnowflake.worktimer.SHORT_BREAK_DURATION
import com.mcsnowflake.worktimer.WORK_SESSION_DURATION
import com.mcsnowflake.worktimer.getDefault
import com.mcsnowflake.worktimer.service.WorkTimer
import java.time.Duration
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map

suspend fun <P> DataStore<Preferences>.set(key: Preferences.Key<P>, value: P) = edit { it[key] = value }
fun <P> DataStore<Preferences>.getAsFlow(key: Preferences.Key<P>) = data.map { it[key] ?: key.getDefault() }
suspend fun <P> DataStore<Preferences>.getOrDefault(key: Preferences.Key<P>) = getAsFlow(key).firstOrNull() ?: key.getDefault()

suspend fun DataStore<Preferences>.getDuration(key: Preferences.Key<Int>): Duration = Duration.ofMinutes(getOrDefault(key).toLong())
suspend fun DataStore<Preferences>.getDurationFor(state: WorkTimer.SessionType): Duration = when (state) {
    WorkTimer.SessionType.WORK_SESSION -> getDuration(WORK_SESSION_DURATION)
    WorkTimer.SessionType.SHORT_BREAK -> getDuration(SHORT_BREAK_DURATION)
    WorkTimer.SessionType.LONG_BREAK -> getDuration(LONG_BREAK_DURATION)
}
