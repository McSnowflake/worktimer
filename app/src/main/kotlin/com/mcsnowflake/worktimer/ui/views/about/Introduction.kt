package com.mcsnowflake.worktimer.ui.views.about

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.ProvideTextStyle
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material.icons.rounded.AutoAwesome
import androidx.compose.material.icons.rounded.Settings
import androidx.compose.material.icons.rounded.Timer
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.BiasAlignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.mcsnowflake.worktimer.ui.components.appbar.AppBar
import com.mcsnowflake.worktimer.ui.views.about.Page.FEATURES
import com.mcsnowflake.worktimer.ui.views.about.Page.GREETER
import com.mcsnowflake.worktimer.ui.views.about.Page.MANUAL
import com.mcsnowflake.worktimer.ui.views.about.Page.POMODORO
import com.mcsnowflake.worktimer.ui.views.dashboard.DASHBOARD

const val SHOW_APP_BAR = "showAppBar"
const val ABOUT_SCREEN = "about/{$SHOW_APP_BAR}"
fun String.showAppBar() = replace("{$SHOW_APP_BAR}", true.toString())
const val HEADER_WEIGHT = 2f
const val TEXT_WEIGHT = 5f

@Composable
fun AboutScreen(navController: NavHostController, showAppBar: Boolean) = Column(
    modifier = Modifier.fillMaxSize()
) {
    val page = remember { mutableStateOf(GREETER) }
    if (showAppBar) {
        AppBar(
            title = "About",
            navigation = {
                IconButton({ navController.popBackStack() }) { Icon(Icons.Rounded.ArrowBack, "Back") }
            }
        )
    }
    ProvideTextStyle(
        TextStyle(
            fontFamily = FontFamily.Serif,
            fontWeight = FontWeight.Normal,
            textAlign = TextAlign.Justify,
            fontSize = when {
                (LocalConfiguration.current.screenWidthDp <= 330) -> 16.sp
                (LocalConfiguration.current.screenWidthDp <= 390) -> 18.sp
                else -> 20.sp
            }
        )
    ) {
        Page(navController, page) {
            Box(
                Modifier.weight(1f)
            ) {
                Animated(page.value == GREETER) { this@Page.Greeter(HEADER_WEIGHT, TEXT_WEIGHT) }
                Animated(page.value == POMODORO) { this@Page.Pomodoro(HEADER_WEIGHT, TEXT_WEIGHT) }
                Animated(page.value == MANUAL) { this@Page.Manual(HEADER_WEIGHT, TEXT_WEIGHT) }
                Animated(page.value == FEATURES) { this@Page.Features(HEADER_WEIGHT, TEXT_WEIGHT) }
            }
        }
    }
}

@Composable
fun Animated(isVisible: Boolean, content: @Composable ColumnScope.() -> Unit) =
    AnimatedVisibility(
        visible = isVisible,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            content = content
        )
    }

@Composable
fun ColumnScope.Scrollable(
    weight: Float,
    content: @Composable ColumnScope.() -> Unit
) {
    val scrollState = rememberScrollState()
    if (scrollState.value > 0) Divider()
    Column(
        modifier = Modifier
            .weight(weight)
            .verticalScroll(scrollState),
        verticalArrangement = Arrangement.spacedBy(16.dp, BiasAlignment.Vertical(-0.4f)),
        horizontalAlignment = Alignment.CenterHorizontally,
        content = content
    )
    if (scrollState.value < scrollState.maxValue) Divider()
}

enum class Page(
    val icon: ImageVector?,
    val nextPageLabel: String
) {
    GREETER(null, "Learn more"),
    POMODORO(Icons.Rounded.Timer, "How to use the app"),
    MANUAL(Icons.Rounded.Settings, "Additional Features"),
    FEATURES(Icons.Rounded.AutoAwesome, "");

    fun next(): Page {
        val nextPageOrdinal = ordinal + 1
        return if (nextPageOrdinal == values().size) {
            values()[0]
        } else {
            values()[nextPageOrdinal]
        }
    }

    val isNotLastPage: Boolean get() = this.ordinal < values().size - 1
}

@Composable
fun Header(
    title: String,
    icon: ImageVector,
    modifier: Modifier
) = Row(
    modifier.fillMaxWidth(),
    horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterHorizontally),
    verticalAlignment = Alignment.CenterVertically
) {
    Icon(icon, null, Modifier.size(64.dp))
    Text(
        text = title,
        color = MaterialTheme.colors.primary,
        fontFamily = FontFamily.Serif,
        style = MaterialTheme.typography.h4
    )
}

@Composable
fun Page(
    navController: NavHostController,
    pageState: MutableState<Page>,
    content: @Composable ColumnScope.() -> Unit
) = Surface(Modifier.fillMaxSize()) {
    val padding = if (LocalConfiguration.current.screenWidthDp <= 600) 8.dp else 16.dp
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = padding, vertical = padding.times(2)),
        verticalArrangement = Arrangement.spacedBy(padding),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
        Row(
            modifier = Modifier
                .wrapContentHeight()
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterHorizontally)
        ) {
            SpecialButton(
                modifier = Modifier.weight(0.4f),
                text = if (pageState.value.isNotLastPage) "Skip introduction" else "Close"
            ) { navController.navigate(DASHBOARD) { popUpTo(ABOUT_SCREEN) { inclusive = true } } }
            if (pageState.value.isNotLastPage) {
                SpecialButton(
                    modifier = Modifier.weight(0.4f),
                    text = pageState.value.nextPageLabel
                ) { pageState.value = pageState.value.next() }
            }
        }
    }
}

@Composable
fun SpecialButton(
    modifier: Modifier,
    text: String,
    onClick: () -> Unit
) = OutlinedButton(
    modifier = modifier,
    onClick = onClick,
    contentPadding = PaddingValues(
        start = 8.dp,
        top = 0.dp,
        end = 8.dp,
        bottom = 0.dp
    )
) {
    Text(text, Modifier.padding(bottom = 2.dp, start = 4.dp))
}
