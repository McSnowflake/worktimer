package com.mcsnowflake.worktimer.ui.components.appbar

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.Divider
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.mcsnowflake.worktimer.ui.dialogs.BugReportDialog
import com.mcsnowflake.worktimer.ui.views.about.ABOUT_SCREEN
import com.mcsnowflake.worktimer.ui.views.about.showAppBar
import com.mcsnowflake.worktimer.ui.views.credits.CREDITS

@Preview(showBackground = true, backgroundColor = 0xFF00FF00)
@Composable
fun Menu(
    navigate: (String) -> Unit = {}
) {
    val expandMenu = remember { mutableStateOf(false) }
    val showBugReportDialog = remember { mutableStateOf(false) }

    Box(
        Modifier.wrapContentSize(Alignment.TopEnd)
    ) {
        IconButton({ expandMenu.value = true }) { Icon(Icons.Filled.MoreVert, "Menu") }
    }

    DropdownMenu(
        expanded = expandMenu.value,
        onDismissRequest = { expandMenu.value = false },
    ) {
        DropdownMenuItem(onClick = {
            expandMenu.value = false
            navigate(ABOUT_SCREEN.showAppBar())
        }) {
            Text("About the app")
        }
        Divider()
        DropdownMenuItem(onClick = {
            expandMenu.value = false
            navigate(CREDITS)
        }) {
            Text("Credits")
        }
        Divider()
        DropdownMenuItem(onClick = {
            expandMenu.value = false
            showBugReportDialog.value = true
        }) {
            Text("Report a bug")
        }
    }

    BugReportDialog(showBugReportDialog)
}
