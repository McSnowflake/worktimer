package com.mcsnowflake.worktimer.ui.components.banners

import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.NotificationsOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview

@Preview
@Composable
fun NotificationBanner(
    isVisible: MutableState<Boolean> = mutableStateOf(true),
    color: Color = MaterialTheme.colors.surface,
    unmuteAction: () -> Unit = {}
) = AnimatedBanner(
    isVisible,
    color,
    { Icon(Icons.Rounded.NotificationsOff, "muted") },
    "No permission to show notifications",
    "ALLOW",
    unmuteAction
)
