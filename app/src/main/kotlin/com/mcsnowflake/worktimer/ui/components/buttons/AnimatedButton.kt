package com.mcsnowflake.worktimer.ui.components.buttons

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandIn
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkOut
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp

@Composable
fun AnimatedButton(
    isVisible: State<Boolean>,
    label: String,
    onClick: () -> Unit,
    colors: ButtonColors = ButtonDefaults.buttonColors(),
    enabled: State<Boolean> = remember { mutableStateOf(true) },
) = AnimatedVisibility(
    isVisible.value,
    enter = fadeIn() + expandIn(initialSize = { IntSize(it.width, 0) }),
    exit = shrinkOut(targetSize = { IntSize(it.width, 0) }) + fadeOut()
) {
    Button(onClick, Modifier.width(200.dp), enabled.value, colors = colors) { Text(label) }
}
