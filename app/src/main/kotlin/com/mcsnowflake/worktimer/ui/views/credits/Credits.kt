package com.mcsnowflake.worktimer.ui.views.credits

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DesignServices
import androidx.compose.material.icons.filled.Extension
import androidx.compose.material.icons.outlined.Lightbulb
import androidx.compose.material.icons.outlined.MusicNote
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.UrlAnnotation
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.mcsnowflake.worktimer.ui.components.appbar.SimpleAppBar
import com.mikepenz.aboutlibraries.ui.compose.LibrariesContainer

const val CREDITS = "credits"

@Preview
@Composable
fun Credits(navigateBack: () -> Unit = {}) {

    var tabIndex by remember { mutableStateOf(0) }
    val tabs = listOf("Creators", "Libraries")

    Column(modifier = Modifier.fillMaxWidth()) {
        SimpleAppBar("Credits", navigateBack)
        TabRow(selectedTabIndex = tabIndex) {
            tabs.forEachIndexed { index, title ->
                Tab(text = { Text(title) },
                    selected = tabIndex == index,
                    onClick = { tabIndex = index },
                    icon = {
                        when (index) {
                            0 -> Icon(imageVector = Icons.Default.DesignServices, contentDescription = null)
                            1 -> Icon(imageVector = Icons.Default.Extension, contentDescription = null)
                        }
                    }
                )
            }
        }
        when (tabIndex) {
            0 -> Creators()
            1 -> LibrariesContainer(Modifier.fillMaxSize())
        }
    }
}

@Composable
fun Creators() = Surface {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CreditCard(
            "Pomodoro Principle",
            Icons.Outlined.Lightbulb,
            "The Pomodoro Technique is a time management method developed by Francesco Cirillo in the late 1980s.",
            "francescocirillo.com/products/the-pomodoro-technique"
        )
        Divider()
        CreditCard(
            "Sound Design",
            Icons.Outlined.MusicNote,
            "Sounds designed and provided by Marius Lex.",
            "lex-filmton.de"
        )
        Divider()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
fun CreditCard(
    title: String,
    icon: ImageVector,
    description: String,
    link: String
) = Column(
    modifier = Modifier.fillMaxWidth().padding(16.dp),
    verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    val uriHandler = LocalUriHandler.current

    Row(
        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterHorizontally),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(icon, title, Modifier.size(32.dp))
        Text(
            text = title,
            color = MaterialTheme.colors.primary,
            fontFamily = FontFamily.Serif,
            style = MaterialTheme.typography.h5
        )
    }
    Spacer(modifier = Modifier.height(4.dp))
    Text(text = description, textAlign = TextAlign.Center)
    ClickableText(
        style = TextStyle(
            color = MaterialTheme.colors.secondary,
            fontSize = 18.sp,
            textDecoration = TextDecoration.Underline,
            textAlign = TextAlign.Center
        ),
        text = buildAnnotatedString {
            append(link)


            addUrlAnnotation(
                urlAnnotation = UrlAnnotation(link),
                start = 0,
                end = link.length
            )
        },
        onClick = { uriHandler.openUri(link) }
    )
}
