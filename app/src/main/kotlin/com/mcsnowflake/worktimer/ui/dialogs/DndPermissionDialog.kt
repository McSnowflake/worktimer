package com.mcsnowflake.worktimer.ui.dialogs

import android.content.Intent
import android.provider.Settings
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Preview(widthDp = 250)
@Composable
fun DndPermissionDialog(
    startActivity: (Intent) -> Unit = {},
    onDismiss: () -> Unit = {}
) {
    AlertDialog(
        onDismissRequest = { onDismiss() },
        dismissButton = { Button({ onDismiss() }) { Text("Cancel") } },
        confirmButton = {
            Button({
                onDismiss()
                startActivity(Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS))
            }) { Text("Ok") }
        },
        title = { Text("Permissions required") },
        text = {
            Column(verticalArrangement = Arrangement.spacedBy(4.dp)) {
                Text(
                    "In order to change the mute state (also called DO-NOT-DISTURB mode), the app needs according permissions."
                )
                Text(
                    "You will be forwarded to the settings page. There, you need to find the WorkTimer, allow the permission and come back here."
                )
            }
        }
    )
}
