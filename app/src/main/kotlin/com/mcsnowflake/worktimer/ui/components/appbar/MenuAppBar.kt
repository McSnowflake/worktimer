package com.mcsnowflake.worktimer.ui.components.appbar

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.QueryStats
import androidx.compose.material.icons.rounded.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.mcsnowflake.worktimer.PREFERENCES_SCREEN
import com.mcsnowflake.worktimer.ui.views.STATS_VIEW

@Preview
@Composable
fun MenuAppBar(
    title: String = "Preview",
    navigateTo: (String) -> Unit = {},
    showStatsIcon: Boolean = true
) = AppBar(
    title = title,
    navigation = { },
    actionIcons = {
        if (showStatsIcon)
            IconButton({ navigateTo(STATS_VIEW) }) { Icon(Icons.Rounded.QueryStats, "Statistics") }
        IconButton({ navigateTo(PREFERENCES_SCREEN) }) { Icon(Icons.Rounded.Settings, "Settings") }
    },
    menu = { Menu(navigateTo) }
)
