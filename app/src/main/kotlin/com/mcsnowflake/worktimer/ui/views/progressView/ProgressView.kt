package com.mcsnowflake.worktimer.ui.views.progressView

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.mcsnowflake.worktimer.ui.views.dashboard.DashBoardViewModel
import com.mcsnowflake.worktimer.ui.components.buttons.ActionButtons

@Composable
fun ProgressView(
    modifier: Modifier = Modifier,
    dashBoardViewModel: DashBoardViewModel
) = Column(
    modifier = modifier.fillMaxWidth(),
    verticalArrangement = Arrangement.Top,
    horizontalAlignment = Alignment.CenterHorizontally
) {
    ProgressIndicator(Modifier.height(200.dp), dashBoardViewModel)
    ActionButtons(Modifier.fillMaxSize(), dashBoardViewModel)
}

@Composable
fun ProgressIndicator(modifier: Modifier = Modifier, dashBoardViewModel: DashBoardViewModel) = Column(
    modifier = modifier,
    verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally
) {
    val progress by dashBoardViewModel.progress.collectAsState()
    val remainingTime by dashBoardViewModel.remainingTime.collectAsState()
    val infoText by dashBoardViewModel.infoText.collectAsState()
//    val showAlarmView by timerViewModel.alarmView.collectAsState()
//    val showProgress by timerViewModel.showProgress.collectAsState()
    val isVisible by dashBoardViewModel.isRunning.collectAsState()
    val timerState by dashBoardViewModel.currentSession.collectAsState()

    AnimatedVisibility(
        isVisible && remainingTime.isNotEmpty(),
        enter = expandVertically(expandFrom = Alignment.Top, animationSpec = tween()) + fadeIn(animationSpec = tween()),
        exit = shrinkVertically(shrinkTowards = Alignment.Top, animationSpec = tween()) + fadeOut(animationSpec = tween())
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = "Remaining Time",
            textAlign = TextAlign.Left,
            fontSize = 20.sp,
        )
    }

    AnimatedVisibility(
        isVisible && infoText.isEmpty(),
        enter = fadeIn(animationSpec = tween(200, 100)),
        exit = fadeOut(animationSpec = tween(200))
    ) {
//        val animProgress: State<Float> = animateFloatAsState(
//            targetValue = progress,
//            animationSpec = SpringSpec(
//                dampingRatio = Spring.DampingRatioNoBouncy,
//                stiffness = Spring.StiffnessVeryLow,
//                // The default threshold is 0.01, or 1% of the overall progress range, which is quite
//                // large and noticeable.
//                visibilityThreshold = if (progress > 0.99F || progress < 0.01 ) 1F else 0.001f,
//            ),
//
//        )

        LinearProgressIndicator(
            modifier = Modifier
                .fillMaxWidth()
                .height(5.dp)
            ,
            progress = progress,
            color = if (timerState.isWorkSession()) MaterialTheme.colors.secondaryVariant
            else MaterialTheme.colors.primaryVariant,
            backgroundColor = if (timerState.isWorkSession()) MaterialTheme.colors.primaryVariant.copy(alpha = .2f)
            else MaterialTheme.colors.secondaryVariant.copy(alpha = .2f),
            strokeCap = StrokeCap.Round
        )
    }
    AnimatedVisibility(
        isVisible && infoText.isNotEmpty(),
    ) {

        Text(
            modifier = Modifier.fillMaxWidth(),
            text = infoText,
            textAlign = TextAlign.Center,
            fontSize = 20.sp
        )
    }

    // remaining time
    AnimatedVisibility(
        isVisible && remainingTime.isNotEmpty(),
        enter = expandVertically(expandFrom = Alignment.Bottom, animationSpec = tween()) + fadeIn(animationSpec = tween()),
        exit = shrinkVertically(shrinkTowards = Alignment.Bottom, animationSpec = tween()) + fadeOut(animationSpec = tween())
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = remainingTime,
            textAlign = TextAlign.Right,
            fontSize = 20.sp
        )
    }
}
