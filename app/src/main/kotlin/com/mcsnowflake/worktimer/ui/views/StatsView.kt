package com.mcsnowflake.worktimer.ui.views

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.mcsnowflake.worktimer.ui.components.appbar.SimpleAppBar

const val STATS_VIEW = "statistics"

@Preview
@Composable
fun StatsView(
    navigateBack: () -> Unit = {},
) = Surface {
    Column(modifier = Modifier.fillMaxSize()) {
        SimpleAppBar("Statistics", navigateBack)
    }
}
