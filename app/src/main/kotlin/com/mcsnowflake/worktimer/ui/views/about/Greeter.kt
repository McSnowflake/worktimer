package com.mcsnowflake.worktimer.ui.views.about

import androidx.compose.foundation.Image
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import com.mcsnowflake.worktimer.R

@Composable
fun ColumnScope.Greeter(
    headerWeight: Float,
    textWeight: Float
) {
    Column(
        modifier = Modifier.weight(headerWeight + 1f),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically)
    ) {
        Image(
            modifier = Modifier.weight(1f),
            painter = painterResource(id = R.drawable.ic_logo),
            contentDescription = "Application Logo - a clock",
            alpha = if (isSystemInDarkTheme()) 0.8f else DefaultAlpha
        )
        Text(text = "WorkTimer", style = MaterialTheme.typography.h3, color = MaterialTheme.colors.primary, fontFamily = FontFamily.Serif)
    }
    Scrollable(textWeight) {
        Text("This app supports you in raising focus and productivity by structuring your work time into productivity and relax sessions. ")
        Text("This is known as the pomodoro technique.")
        Text("Additionally, it can remind you to drink and shield you from disturbances by enabling android's do-not-disturb mode.")
    }
}
