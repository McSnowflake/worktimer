package com.mcsnowflake.worktimer.ui.views.dashboard

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcsnowflake.worktimer.OVERTIME_MODE
import com.mcsnowflake.worktimer.STATISTICS_ON
import com.mcsnowflake.worktimer.TextHandler.getFormattedTime
import com.mcsnowflake.worktimer.TextHandler.getInfoText
import com.mcsnowflake.worktimer.TextHandler.getTitle
import com.mcsnowflake.worktimer.misc.getAsFlow
import com.mcsnowflake.worktimer.misc.getOrDefault
import com.mcsnowflake.worktimer.service.TimerAction.MORE
import com.mcsnowflake.worktimer.service.TimerAction.PAUSE
import com.mcsnowflake.worktimer.service.TimerAction.RESUME
import com.mcsnowflake.worktimer.service.TimerAction.WORK
import com.mcsnowflake.worktimer.service.TimerState
import com.mcsnowflake.worktimer.service.TimerState.NotRunning
import com.mcsnowflake.worktimer.service.TimerState.Running
import com.mcsnowflake.worktimer.service.TimerState.Running.Created
import com.mcsnowflake.worktimer.service.TimerState.Running.Finished
import com.mcsnowflake.worktimer.service.TimerState.Running.Progress
import com.mcsnowflake.worktimer.service.TimerState.Running.Started
import com.mcsnowflake.worktimer.service.WithSession
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION
import java.time.Duration
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class DashBoardViewModel(
    private val preferences: DataStore<Preferences>
) : ViewModel() {

    val timerState = MutableStateFlow<TimerState>(NotRunning)
    private val runningStates = timerState.filterIsInstance<Running>()
    val isRunning = timerState.map { it is Running }.stateIn(viewModelScope, Lazily, false)
    val isNotRunning = isRunning.map { !it }.stateIn(viewModelScope, Lazily, true)

    val currentSession: StateFlow<WithSession> = timerState.filterIsInstance<WithSession>().stateIn(
        viewModelScope,
        Lazily,
        Finished(WORK_SESSION)
    )

    // element content
    val infoText: StateFlow<String> = runningStates.map(::getInfoText).distinctUntilChanged().stateIn(
        viewModelScope,
        Lazily,
        ""
    )
    val remainingTime: StateFlow<String> = runningStates.map(::getFormattedTime).distinctUntilChanged().stateIn(
        viewModelScope,
        Lazily,
        ""
    )
    val progress: StateFlow<Float> = timerState.filterIsInstance<Progress>()
        .map { it.progress }.distinctUntilChanged().stateIn(viewModelScope, Lazily, 0F)
    val headerText: StateFlow<String> = timerState
        .map { if (it is Running) getTitle(it) else "Let's get onto it!" }
        .stateIn(viewModelScope, Lazily, "Let's get onto it!")

    // button labels
    val nextButtonLabel: StateFlow<String> = runningStates.map {
        when (it) {
            is Progress ->
                if (it.isWorkSession()) {
                    PAUSE.getActionTitle()
                } else WORK.getActionTitle()

            is Finished -> RESUME.getActionTitle()
            else -> ""
        }
    }.distinctUntilChanged().stateIn(viewModelScope, Lazily, "")
    val moreButtonLabel: StateFlow<String> = runningStates.map {
        when (it) {
            is Created, is Started, is Progress -> "I need more time"
            is Finished -> MORE.getActionTitle()
            else -> ""
        }
    }.distinctUntilChanged().stateIn(viewModelScope, Lazily, "")

    // element visibilities
    // val alarmView: StateFlow<Boolean> = currentSession.map { it is Finished }.distinctUntilChanged().stateIn(viewModelScope, Lazily, false)
    // val showProgress: StateFlow<Boolean> = currentSession.map { it !is Finished }.distinctUntilChanged().stateIn(viewModelScope, Lazily, false)
    val showMoreButton: StateFlow<Boolean> = timerState.getMapped {
        when (it) {
            is Progress -> preferences.getOrDefault(OVERTIME_MODE) && it.remainingTime < Duration.ofMinutes(5)
            is Finished -> preferences.getOrDefault(OVERTIME_MODE)
            else -> false
        }
    }
    val showNextButton: StateFlow<Boolean> = nextButtonLabel.getMapped { it.isNotEmpty() }

    val showStatsIcon: Flow<Boolean> = preferences.getAsFlow(STATISTICS_ON)

    // Banners
    private val reevaluateBanners = MutableStateFlow(0)

    @Composable
    fun getReevaluateBanners() = reevaluateBanners.collectAsState(viewModelScope)
    fun setReevaluateBanners() = viewModelScope.launch { reevaluateBanners.emit(reevaluateBanners.value + 1) }

    private fun <T> StateFlow<T>.getMapped(transform: suspend (value: T) -> Boolean) = map(transform).distinctUntilChanged().stateIn(
        viewModelScope,
        Lazily,
        false
    )
}
