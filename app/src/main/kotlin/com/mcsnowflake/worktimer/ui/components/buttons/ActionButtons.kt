package com.mcsnowflake.worktimer.ui.components.buttons

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.mcsnowflake.worktimer.service.TimerAction.STOP
import com.mcsnowflake.worktimer.service.TimerActionEmitter
import com.mcsnowflake.worktimer.service.TimerService.Command
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION
import com.mcsnowflake.worktimer.ui.views.dashboard.DashBoardViewModel
import org.koin.compose.koinInject

@Composable
fun ActionButtons(
    modifier: Modifier = Modifier,
    dashBoardViewModel: DashBoardViewModel,
    timerTrigger: TimerActionEmitter = koinInject()
) = Box(
    modifier = modifier.fillMaxWidth(),
    contentAlignment = Alignment.Center
) {
    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val nextLabel by dashBoardViewModel.nextButtonLabel.collectAsState()
        val moreLabel by dashBoardViewModel.moreButtonLabel.collectAsState()
        val timerState by dashBoardViewModel.currentSession.collectAsState()
        val timerRunning = dashBoardViewModel.isRunning.collectAsState()

        AnimatedButton(
            dashBoardViewModel.showNextButton.collectAsState(),
            nextLabel,
            { timerTrigger.emit(Command.NEXT) },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = if (timerState.sessionType == WORK_SESSION) MaterialTheme.colors.secondary
                else MaterialTheme.colors.primary
            )
        )

        AnimatedButton(
            dashBoardViewModel.showMoreButton.collectAsState(),
            moreLabel,
            { timerTrigger.emit(Command.MORE) },
            ButtonDefaults.buttonColors(
                backgroundColor = if (timerState.sessionType == WORK_SESSION) MaterialTheme.colors.primary
                else MaterialTheme.colors.secondary
            )
        )

        AnimatedButton(
            timerRunning,
            STOP.getActionTitle(),
            { timerTrigger.emit(Command.STOP) },
            ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondaryVariant)
        )

        Spacer(modifier = Modifier.height(0.dp))
    }
}
