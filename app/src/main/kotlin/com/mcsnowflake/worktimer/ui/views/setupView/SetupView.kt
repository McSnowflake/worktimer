package com.mcsnowflake.worktimer.ui.views.setupView

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ContentAlpha
import androidx.compose.material.Divider
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.mcsnowflake.worktimer.LONG_BREAK_DURATION
import com.mcsnowflake.worktimer.SHORT_BREAK_DURATION
import com.mcsnowflake.worktimer.SHORT_BREAK_REPETITIONS
import com.mcsnowflake.worktimer.WORK_SESSION_DURATION
import com.mcsnowflake.worktimer.service.TimerAction
import com.mcsnowflake.worktimer.service.TimerActionEmitter
import com.mcsnowflake.worktimer.service.TimerService
import com.mcsnowflake.worktimer.ui.components.buttons.AnimatedButton
import org.koin.androidx.compose.getViewModel
import org.koin.compose.koinInject

@Composable
fun SetupView(
    modifier: Modifier = Modifier,
    isVisible: State<Boolean>
) = Column(
    modifier = modifier.fillMaxSize(),
    verticalArrangement = Arrangement.Top,
    horizontalAlignment = Alignment.CenterHorizontally
) {
    InputFields(Modifier.height(200.dp), isVisible)
    StartButton(Modifier.weight(5f), isVisible)
}

@Composable
fun InputFields(
    modifier: Modifier = Modifier,
    isVisible: State<Boolean>,
    viewModel: SetupViewModel = getViewModel()
) = Column(
    modifier = modifier
        .fillMaxWidth()
        .wrapContentHeight(),
    verticalArrangement = Arrangement.spacedBy(8.dp)
) {

    AnimatedVisibility(
        isVisible.value,
        enter = expandVertically(expandFrom = Alignment.Top, animationSpec = tween()) + fadeIn(animationSpec = tween()),
        exit = shrinkVertically(shrinkTowards = Alignment.Top, animationSpec = tween()) + fadeOut(animationSpec = tween())
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(32.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            NumberInput(
                value = viewModel[WORK_SESSION_DURATION],
                onValueChange = { viewModel[WORK_SESSION_DURATION] = it.trim() },
                modifier = Modifier.weight(1F),
                label = { Text(text = "work session") },
                visualTransformation = SuffixTransformation(" min"),
                accentColor = MaterialTheme.colors.primaryVariant
            )
            NumberInput(
                value = viewModel[SHORT_BREAK_REPETITIONS],
                onValueChange = { viewModel[SHORT_BREAK_REPETITIONS] = it.trim() },
                modifier = Modifier.weight(1F),
                label = { Text(text = "break ratio") },
                visualTransformation = SuffixTransformation(" : 1"),
                accentColor = MaterialTheme.colors.secondaryVariant
            )
        }
    }
    if (isVisible.value) Divider(color = MaterialTheme.colors.secondaryVariant.copy(alpha = .2f), modifier = Modifier.padding(top = 6.dp))
    AnimatedVisibility(
        isVisible.value,
        enter = expandVertically(expandFrom = Alignment.Bottom, animationSpec = tween()) + fadeIn(animationSpec = tween()),
        exit = shrinkVertically(shrinkTowards = Alignment.Bottom, animationSpec = tween()) + fadeOut(animationSpec = tween())
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(32.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            NumberInput(
                value = viewModel[SHORT_BREAK_DURATION],
                onValueChange = { viewModel[SHORT_BREAK_DURATION] = it.trim() },
                modifier = Modifier.weight(1F),
                label = { Text(text = "short break") },
                visualTransformation = SuffixTransformation(" min"),
                accentColor = MaterialTheme.colors.secondaryVariant
            )
            NumberInput(
                value = viewModel[LONG_BREAK_DURATION],
                onValueChange = { viewModel[LONG_BREAK_DURATION] = it.trim() },
                modifier = Modifier.weight(1F),
                label = { Text(text = "long break") },
                visualTransformation = SuffixTransformation(" min"),
                accentColor = MaterialTheme.colors.secondaryVariant
            )
        }
    }
}

@Composable
fun StartButton(
    modifier: Modifier = Modifier,
    isVisible: State<Boolean>,
    timerTrigger: TimerActionEmitter = koinInject(),
    viewModel: SetupViewModel = viewModel()
) = Box(
    modifier = modifier.fillMaxWidth(),
    contentAlignment = Alignment.Center
) {
    AnimatedButton(
        isVisible,
        TimerAction.START.getActionTitle(),
        { timerTrigger.emit(TimerService.Command.START) },
        enabled = viewModel.isInputValid.collectAsState()
    )
}

@Composable
private fun NumberInput(
    value: State<String>,
    onValueChange: (value: String) -> Unit,
    modifier: Modifier = Modifier,
    label: @Composable () -> Unit,
    visualTransformation: VisualTransformation,
    accentColor: Color
) = OutlinedTextField(
    value = value.value,
    onValueChange = onValueChange,
    modifier = modifier,
    keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
    label = label,
    visualTransformation = visualTransformation,
    colors = TextFieldDefaults.outlinedTextFieldColors(
        cursorColor = accentColor,
        focusedBorderColor = accentColor.copy(alpha = ContentAlpha.high),
        focusedLabelColor = accentColor.copy(alpha = ContentAlpha.high),
    ),
    textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.Center)
)

class SuffixTransformation(private val suffix: String) : VisualTransformation {

    override fun filter(text: AnnotatedString) = TransformedText(
        AnnotatedString(text.text + suffix),
        object : OffsetMapping {
            override fun originalToTransformed(offset: Int) = offset
            override fun transformedToOriginal(offset: Int) =
                if (offset < text.text.length) offset
                else maxOf(text.text.length, offset - suffix.length)
        }
    )
}
