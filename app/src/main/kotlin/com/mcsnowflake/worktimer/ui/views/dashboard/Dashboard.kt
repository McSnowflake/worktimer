package com.mcsnowflake.worktimer.ui.views.dashboard

import android.Manifest
import android.app.NotificationManager
import android.media.AudioManager
import android.os.Build
import androidx.activity.result.ActivityResultLauncher
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.mcsnowflake.worktimer.ui.components.appbar.MenuAppBar
import com.mcsnowflake.worktimer.ui.components.banners.MuteBanner
import com.mcsnowflake.worktimer.ui.components.banners.NotificationBanner
import com.mcsnowflake.worktimer.ui.views.progressView.ProgressView
import com.mcsnowflake.worktimer.ui.views.setupView.SetupView
import org.koin.androidx.compose.getViewModel
import org.koin.compose.koinInject

const val DASHBOARD = "dashboard"

// @Preview
@Composable
fun Dashboard(
    modifier: Modifier = Modifier,
    navigateTo: (String) -> Unit = {},
    dashBoardViewModel: DashBoardViewModel = getViewModel(),
    notificationManager: NotificationManager = koinInject(),
    audioManager: AudioManager = koinInject(),
    permissionRequestLauncher: ActivityResultLauncher<String>,
    showDndDialog: () -> Unit
) = Column(modifier.fillMaxSize()) {
    val showStatsIcon by dashBoardViewModel.showStatsIcon.collectAsState(false)
    MenuAppBar("WorkTimer", navigateTo, showStatsIcon)
    Surface(
        Modifier.fillMaxSize(),
        color = MaterialTheme.colors.background
    ) {
        Column {
            val showMuteBanner = remember { mutableStateOf(false) }
            val showNotificationBanner = remember { mutableStateOf(false) }
            val reevaluate by dashBoardViewModel.getReevaluateBanners()

            LaunchedEffect(audioManager.ringerMode, reevaluate) {
                showMuteBanner.value = audioManager.ringerMode != AudioManager.RINGER_MODE_NORMAL && notificationManager.areNotificationsEnabled()
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                LaunchedEffect(reevaluate) {
                    showNotificationBanner.value = !notificationManager.areNotificationsEnabled()
                    if (showNotificationBanner.value) showMuteBanner.value = false
                }
                NotificationBanner(showNotificationBanner) {
                    permissionRequestLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                }
            }
            MuteBanner(showMuteBanner) {
                if (!notificationManager.isNotificationPolicyAccessGranted) {
                    showDndDialog()
                } else {
                    audioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
                    dashBoardViewModel.setReevaluateBanners()
                }
            }

            Column(
                Modifier
                    .fillMaxSize()
                    .padding(32.dp)
            ) {
                Header(Modifier.weight(3F), dashBoardViewModel.headerText.collectAsState())
                Box(Modifier.weight(10F)) {
                    SetupView(Modifier.fillMaxSize(), dashBoardViewModel.isNotRunning.collectAsState())
                    ProgressView(Modifier.fillMaxSize(), dashBoardViewModel)
                }
            }
        }
    }
}

@Composable
fun Header(modifier: Modifier = Modifier, headerText: State<String>) = Box(modifier.fillMaxWidth(), Alignment.Center) {
    Text(headerText.value, fontSize = 30.sp)
}
