package com.mcsnowflake.worktimer.ui.dialogs

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Preview
@Composable
fun BugReportDialog(showDialog: MutableState<Boolean> = mutableStateOf(true)) {
    if (showDialog.value) {
        val uriHandler = LocalUriHandler.current
        AlertDialog(
            onDismissRequest = {
            },
            title = {
                Text("Report a Bug")
            },
            confirmButton = {
                Button(
                    onClick = {
                        showDialog.value = false
                        uriHandler.openUri("https://gitlab.com/McSnowflake/worktimer/-/issues")
                    },
                ) {
                    Text("Ok")
                }
            },
            dismissButton = {
                Button(
                    onClick = { showDialog.value = false },
                ) {
                    Text("Decline")
                }
            },
            text = {
                Column(
                    verticalArrangement = Arrangement.spacedBy(4.dp)
                ) {
                    Text("You will be forwarded to GitLab, where bugs are tracked for this project.")
                    Text("Mind: in order to file a bug, you need to be registered there.")
                    Text("Please check if there already is a bug reporting your problem. If so, add to it instead of creating a new one.")
                }
            },
        )
    }
}
