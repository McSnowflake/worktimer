package com.mcsnowflake.worktimer.ui.views.about

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.mcsnowflake.worktimer.ui.views.about.InputFields.BREAK_RATIO
import com.mcsnowflake.worktimer.ui.views.about.InputFields.LONG_BREAK
import com.mcsnowflake.worktimer.ui.views.about.InputFields.SHORT_BREAK
import com.mcsnowflake.worktimer.ui.views.about.InputFields.WORK_SESSION

@Composable
fun ColumnScope.Manual(
    headerWeight: Float,
    textWeight: Float
) {
    Header(
        "Customizable",
        Icons.Rounded.Settings,
        Modifier.weight(headerWeight)
    )
    Scrollable(textWeight) {
        Text("Although the pomodoro technique has some sane propositions for the default values, you can of course adjust the work timer to your individual needs.")
        Text("On the starting screen you will find the following four inputs:")
        val selection = remember { mutableStateOf(WORK_SESSION) }
        Row(
            modifier = Modifier.height(IntrinsicSize.Min),
            horizontalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            DemoInput(WORK_SESSION, MaterialTheme.colors.primary, selection)
            DemoInput(BREAK_RATIO, MaterialTheme.colors.secondary, selection)
            DemoInput(SHORT_BREAK, MaterialTheme.colors.secondaryVariant, selection)
            DemoInput(LONG_BREAK, MaterialTheme.colors.secondaryVariant, selection)
        }
        Box(modifier = Modifier.wrapContentHeight()) {
            when (selection.value) {
                WORK_SESSION -> Text("A work session (also called a pomodoro) is an interval of work time, typically 25 minutes")
                SHORT_BREAK -> Text("Short breaks are usually 5 - 10 minutes.")
                LONG_BREAK -> Text("Longer breaks take 20 - 30 minutes.")
                BREAK_RATIO -> Text("The break ratio lets you set how many short breaks you have before a longer one occurs.")
            }
        }
    }
}

enum class InputFields(val label: String) {
    WORK_SESSION("work session"),
    SHORT_BREAK("short break"),
    LONG_BREAK("long break"),
    BREAK_RATIO("break ratio")
}

@Composable
fun RowScope.DemoInput(text: InputFields, color: Color, selected: MutableState<InputFields>, weight: Float = 1f) =
    OutlinedButton(
        { selected.value = text },
        Modifier
            .weight(weight)
            .fillMaxHeight(),
        colors = ButtonDefaults.outlinedButtonColors(contentColor = MaterialTheme.colors.onSurface),
        border = BorderStroke(
            2.dp,
            if (selected.value == text) color
            else color.copy(alpha = ButtonDefaults.OutlinedBorderOpacity)
        )
    ) {
        Text(
            text.label,
            style = MaterialTheme.typography.body2,
            textAlign = TextAlign.Center
        )
    }
