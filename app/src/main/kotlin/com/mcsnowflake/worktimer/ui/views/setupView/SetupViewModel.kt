package com.mcsnowflake.worktimer.ui.views.setupView

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.core.text.isDigitsOnly
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcsnowflake.worktimer.LONG_BREAK_DURATION
import com.mcsnowflake.worktimer.SHORT_BREAK_DURATION
import com.mcsnowflake.worktimer.SHORT_BREAK_REPETITIONS
import com.mcsnowflake.worktimer.WORK_SESSION_DURATION
import com.mcsnowflake.worktimer.misc.getAsFlow
import com.mcsnowflake.worktimer.misc.set
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted.Companion.Eagerly
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class SetupViewModel(preferences: DataStore<Preferences>) : ViewModel() {

    private val inputs = mapOf(
        WORK_SESSION_DURATION to MutableStateFlow(""),
        SHORT_BREAK_REPETITIONS to MutableStateFlow(""),
        SHORT_BREAK_DURATION to MutableStateFlow(""),
        LONG_BREAK_DURATION to MutableStateFlow("")
    )

    init {
        inputs.forEach {
            // write back inputs
            viewModelScope.launch {
                it.value
                    .filter { it.isDigitsOnly() && it.isNotEmpty() }
                    .map { it.toInt() }
                    .collect { input -> preferences.set(it.key, input) }
            }

            // Load initial value
            viewModelScope.launch { it.value.emit(preferences.getAsFlow(it.key).first().toString()) }
        }
    }

    @Composable
    operator fun get(key: Preferences.Key<Int>): State<String> = inputs[key]!!.asStateFlow().collectAsState()
    operator fun set(key: Preferences.Key<Int>, value: String) = viewModelScope.launch { inputs[key]!!.emit(value) }

    val isInputValid = combine(inputs.values) { inputs: Array<String> -> inputs.all { it.isDigitsOnly() && it.isNotEmpty() && it.toInt() > 0 } }
        .stateIn(viewModelScope, Eagerly, false)
}
