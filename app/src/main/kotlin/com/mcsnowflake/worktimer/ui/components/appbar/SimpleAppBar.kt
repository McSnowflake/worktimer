package com.mcsnowflake.worktimer.ui.components.appbar

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview

@Preview
@Composable
fun SimpleAppBar(
    title: String = "Preview",
    navigateBack: () -> Unit = {}
) = AppBar(
    title = title,
    navigation = { IconButton(navigateBack) { Icon(Icons.Rounded.ArrowBack, "Back") } }
)
