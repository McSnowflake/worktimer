package com.mcsnowflake.worktimer.ui.views.about

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.contentColorFor
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Timer
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.sp

@Composable
fun ColumnScope.Pomodoro(
    headerWeight: Float,
    textWeight: Float
) {
    Header(
        "The\nPomodoro\nTechnique",
        Icons.Rounded.Timer,
        Modifier.weight(headerWeight)
    )

    Scrollable(textWeight) {
        Text("The Pomodoro Technique is a time management method developed by Francesco Cirillo in the late 1980s.")
        Text("One goal of the technique is to reduce the effect of internal and external interruptions on focus and flow.")
        Text("It uses a timer to break work into intervals, separated by breaks.")
        Text("Each interval is known as a pomodoro, from the Italian word for tomato (because Cirillo used a kitchen timer).")
        Text("For each pomodoro, you decide beforehand on the task to be done.")
        val uriHandler = LocalUriHandler.current
        val linkStyle = SpanStyle(
            fontSize = 12.sp,
            color = MaterialTheme.colors.secondary,
            textDecoration = TextDecoration.Underline
        )
        val textStyle = SpanStyle(
            fontSize = 12.sp,
            color = if (isSystemInDarkTheme()) Color.White
            else contentColorFor(MaterialTheme.colors.background)
        )
        val annotatedString1 = buildAnnotatedString {
            withStyle(textStyle) { append("Excerpt from the ") }
            pushStringAnnotation(
                tag = "pomodoro",
                annotation = "https://en.wikipedia.org/wiki/Pomodoro_Technique"
            )
            withStyle(linkStyle) { append("Wikipedia article") }
            pop()
        }
        ClickableText(
            text = annotatedString1,
            modifier = Modifier.fillMaxWidth(),
            style = TextStyle(fontSize = 12.sp, textAlign = TextAlign.Center),
            onClick = { offset ->
                annotatedString1.getStringAnnotations("pomodoro", offset, offset).firstOrNull()
                    ?.let { uriHandler.openUri(it.item) }
            }
        )
    }
}
