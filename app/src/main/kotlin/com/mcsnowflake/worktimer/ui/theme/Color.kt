package com.mcsnowflake.worktimer.ui.theme

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

val LightColorPalette = lightColors(
    primary = Color(0xFFf08e1d),
    primaryVariant = Color(0xFFea7f1a),
    secondary = Color(0xFF1d7ff0),
    secondaryVariant = Color(0xFF206ddc),
    onSecondary = Color.White
)

val DarkColorPalette = darkColors(
    primary = Color(0xFFf7b756),
    primaryVariant = Color(0xFFf6a735),
    secondary = Color(0xFF5dafff),
    secondaryVariant = Color(0xFF399eff)
)
