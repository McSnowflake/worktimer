package com.mcsnowflake.worktimer.ui.views.settings

import android.app.NotificationManager
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.DoNotDisturb
import androidx.compose.material.icons.rounded.MoreTime
import androidx.compose.material.icons.rounded.QueryStats
import androidx.compose.material.icons.rounded.Redo
import androidx.compose.material.icons.rounded.WaterDrop
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.datastore.preferences.core.Preferences
import com.mcsnowflake.worktimer.AUTO_SWITCH_MODE
import com.mcsnowflake.worktimer.DND_MODE
import com.mcsnowflake.worktimer.HYDRATION_MODE
import com.mcsnowflake.worktimer.OVERTIME_MODE
import com.mcsnowflake.worktimer.STATISTICS_ON
import com.mcsnowflake.worktimer.ui.components.appbar.SimpleAppBar
import org.koin.androidx.compose.getViewModel
import org.koin.compose.koinInject

@Preview
@Composable
fun Settings(
    showDndDialog: () -> Unit = {}, navigateBack: () -> Unit = {}, preferences: SettingsViewModel = getViewModel(), notificationManager: NotificationManager = koinInject()
) = Surface(
    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background
) {
    Column(modifier = Modifier.fillMaxSize()) {

        SimpleAppBar("Settings", navigateBack)

        val preferencesFullyLoaded by preferences.allLoaded.collectAsState()

        Column {
            SettingsCard(
                key = AUTO_SWITCH_MODE, preferences = preferences, enabled = preferencesFullyLoaded
            )
            Divider()
            SettingsCard(
                key = OVERTIME_MODE, preferences = preferences, enabled = preferencesFullyLoaded
            )
            Divider()
            SettingsCard(
                key = DND_MODE, preferences = preferences, enabled = preferencesFullyLoaded
            ) {
                when {
                    it && !notificationManager.isNotificationPolicyAccessGranted -> showDndDialog()
                    else -> preferences[DND_MODE] = it
                }
            }

            Divider()
            SettingsCard(
                key = HYDRATION_MODE, preferences = preferences, enabled = preferencesFullyLoaded
            )
            Divider()
            SettingsCard(
                key = STATISTICS_ON, preferences = preferences, enabled = preferencesFullyLoaded
            )
            Divider()
        }
    }
}

data class SettingsCardData(
    val icon: ImageVector, val label: String, val onText: String, val offText: String
)

val settingsCardsData: Map<String, SettingsCardData> = mapOf(
    AUTO_SWITCH_MODE.name to SettingsCardData(
        icon = Icons.Rounded.Redo,
        label = "Automatic session switching",
        onText = "App switches automatically to the next session",
        offText = "App waits for your approval to switch to the next session"
    ), OVERTIME_MODE.name to SettingsCardData(
        icon = Icons.Rounded.MoreTime,
        label = "Optional session prolonging",
        onText = "An additional button allows to extend the current session by 5 min",
        offText = "Session time is fix once the timer has started",
    ), DND_MODE.name to SettingsCardData(
        icon = Icons.Rounded.DoNotDisturb,
        label = "No Distractions",
        onText = "Do Not Disturb mode will be enabled during work sessions",
        offText = "Do Not Disturb will not be enabled during work sessions",
    ), HYDRATION_MODE.name to SettingsCardData(
        icon = Icons.Rounded.WaterDrop,
        label = "Hydration reminders",
        onText = "Regular reminders to drink water will be issued",
        offText = "Hydration reminders are disabled",
    ), STATISTICS_ON.name to SettingsCardData(
        icon = Icons.Rounded.QueryStats,
        label = "Statistics (beta)",
        onText = "Statistical data is stored on the device and can be reviewed in the chart view",
        offText = "Statistical data collection is disabled",
    )
)

@Composable
fun SettingsCard(
    modifier: Modifier = Modifier, key: Preferences.Key<Boolean>, preferences: SettingsViewModel, enabled: Boolean, onChange: (Boolean) -> Unit = { preferences[key] = it }
) = Row(
    modifier = modifier
        .fillMaxWidth()
        .padding(horizontal = 16.dp, vertical = 4.dp)
        .height(SettingsCardHeight),
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(16.dp)
) {
    val status by preferences[key]
    val data: SettingsCardData = settingsCardsData[key.name]!!

    Row(
        Modifier
            .fillMaxHeight()
            .width(SettingsCardInset), Arrangement.Center, Alignment.CenterVertically
    ) {
        Icon(data.icon, "${data.label} icon")
    }


    Column(
        Modifier
            .fillMaxHeight()
            .weight(1f), verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = data.label, style = MaterialTheme.typography.body1
        )
        Text(
            text = if (status) data.onText else data.offText,
            color = Color.Gray,
            fontSize = 12.sp,
            modifier = Modifier.heightIn(min = 25.dp, max = 40.dp),
            style = MaterialTheme.typography.body2
        )
    }
    Row(
        Modifier.fillMaxHeight(), horizontalArrangement = Arrangement.End, verticalAlignment = Alignment.CenterVertically
    ) {
        Switch(
            checked = status, onCheckedChange = onChange, enabled = enabled
        )
    }
}

private val SettingsCardHeight = 80.dp
private val SettingsCardInset = 40.dp
