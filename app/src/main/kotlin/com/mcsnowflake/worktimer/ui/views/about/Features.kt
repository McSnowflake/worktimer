package com.mcsnowflake.worktimer.ui.views.about

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Divider
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.contentColorFor
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AutoAwesome
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle

@Composable
fun ColumnScope.Features(
    headerWeight: Float,
    textWeight: Float
) {

    Header(
        "Additional\nFeatures",
        Icons.Rounded.AutoAwesome,
        Modifier.weight(headerWeight)
    )

    Scrollable(textWeight) {
        Text("If you check out the in-app settings, you will find the following features.")
        Divider(Modifier.fillMaxWidth(0.6f))
        Text("Auto-Switching: Usually, when a session ends, it will wait for your confirmation, before switching to the next. This mode will do it automatically.")
        Text(
            "Session-Prolonging: If you need more flexibility, you can enable the prolonging-option. " +
                "A new button will allow you to add 5 minutes to your current session if needed."
        )
        Text("The hydration reminder helps you to not forget to drink regularly.")
        val textStyle = SpanStyle(
            color = if (isSystemInDarkTheme()) Color.White
            else contentColorFor(MaterialTheme.colors.background)
        )
        val uriHandler = LocalUriHandler.current
        val linkStyle = SpanStyle(
            color = MaterialTheme.colors.secondary,
            textDecoration = TextDecoration.Underline
        )
        val dndText = buildAnnotatedString {
            withStyle(textStyle) { append("The No-Distractions setting activates an android feature called ") }
            pushStringAnnotation(
                tag = "DnD",
                annotation = "https://support.google.com/android/answer/9069335?hl=en"
            )
            withStyle(linkStyle) { append("DoNotDisturb") }
            pop()
            withStyle(textStyle) { append(" mode during work sessions, that will shield you from disturbances.") }
        }
        ClickableText(
            dndText,
            style = LocalTextStyle.current,
            onClick = { offset -> dndText.getStringAnnotations(offset, offset).firstOrNull()?.let { uriHandler.openUri(it.item) } }
        )
    }
}
