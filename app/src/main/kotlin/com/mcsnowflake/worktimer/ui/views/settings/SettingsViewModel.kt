package com.mcsnowflake.worktimer.ui.views.settings

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcsnowflake.worktimer.PREFERENCES
import com.mcsnowflake.worktimer.misc.getAsFlow
import com.mcsnowflake.worktimer.misc.set
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class SettingsViewModel(preferences: DataStore<Preferences>) : ViewModel() {

    private val inputs = PREFERENCES.keys.associateWith { MutableStateFlow(false) }
    private val isLoading = PREFERENCES.keys.associateWith { MutableStateFlow(true) }

    init {
        inputs.forEach {
            // write back inputs
            viewModelScope.launch { it.value.collect { input -> preferences.set(it.key, input) } }

            // Load initial value
            viewModelScope.launch {
                it.value.emit(preferences.getAsFlow(it.key).first())
                isLoading[it.key]!!.emit(false)
            }
        }
    }

    @Composable
    operator fun get(key: Preferences.Key<Boolean>): State<Boolean> = inputs[key]!!.asStateFlow().collectAsState(viewModelScope.coroutineContext)
    operator fun set(key: Preferences.Key<Boolean>, value: Boolean) = viewModelScope.launch { inputs[key]!!.emit(value) }

    val allLoaded: StateFlow<Boolean> = combine(isLoading.values) { isLoading -> isLoading.none { it } }.stateIn(viewModelScope, Lazily, false)
}
