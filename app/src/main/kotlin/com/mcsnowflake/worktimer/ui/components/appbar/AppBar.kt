package com.mcsnowflake.worktimer.ui.components.appbar

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.SnackbarDefaults
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.contentColorFor
import androidx.compose.runtime.Composable

@Composable
fun AppBar(
    title: String,
    navigation: @Composable () -> Unit,
    actionIcons: @Composable () -> Unit = {},
    menu: @Composable () -> Unit = {},
) {
    TopAppBar(
        title = { Text(title) },
        actions = {
            actionIcons()
            menu()
        },
        navigationIcon = navigation,
        contentColor = if (isSystemInDarkTheme()) MaterialTheme.colors.primary else contentColorFor(SnackbarDefaults.backgroundColor)
    )
}
