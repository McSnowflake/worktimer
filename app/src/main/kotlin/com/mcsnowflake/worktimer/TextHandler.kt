package com.mcsnowflake.worktimer

import android.text.format.DateUtils.formatElapsedTime
import com.mcsnowflake.worktimer.service.TimerState.Running
import com.mcsnowflake.worktimer.service.TimerState.Running.Created
import com.mcsnowflake.worktimer.service.TimerState.Running.Finished
import com.mcsnowflake.worktimer.service.TimerState.Running.Progress
import com.mcsnowflake.worktimer.service.TimerState.Running.Prolonged
import com.mcsnowflake.worktimer.service.TimerState.Running.Started
import com.mcsnowflake.worktimer.service.TimerState.Running.Terminated
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION
import java.time.Duration
import java.util.Locale
import kotlin.math.ceil

object TextHandler { // TODO refactor out string representations

    fun getTitle(state: Running) = when (state) {
        is Started, is Created -> "Starting"
        is Progress ->
            if (state.isWorkSession()) {
                "Work session ongoing"
            } else {
                "Relax time"
            }
        is Prolonged ->
            if (state.isWorkSession()) {
                "Work session ongoing"
            } else {
                "Relax time"
            }
        is Finished ->
            if (state.isWorkSession()) {
                "Work session over"
            } else {
                "Break is over"
            }
        is Terminated -> "Timer Stopped"
    }

    fun getInfoText(state: Running) = when (state) {
        is Started, is Created -> "Timer starting"
        is Progress, is Prolonged -> ""
        is Finished ->
            if (state.sessionType == WORK_SESSION) {
                "Time for a break"
            } else {
                "Get ready to rock"
            }
        is Terminated -> "Timer stopping"
    }

    fun getFormattedTime(event: Running) =
        if (event is Progress) {
            event.remainingTime.format()
        } else {
            ""
        }

    private fun Duration.format(): String {
        val seconds = ceil(this.toMillis().toFloat().div(1000)).toInt()
        return when {
            toHours() >= 1 -> "${formatElapsedTime(this.seconds)} h"
            toMinutes() < 1 -> String.format(Locale.US, "%d sec", seconds)
            else -> String.format(Locale.US, "%d min %d sec", seconds / 60, (seconds % 60))
        }
    }
}
