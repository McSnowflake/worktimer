package com.mcsnowflake.worktimer

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.mcsnowflake.worktimer.service.TimerService
import com.mcsnowflake.worktimer.service.TimerState
import com.mcsnowflake.worktimer.ui.dialogs.DndPermissionDialog
import com.mcsnowflake.worktimer.ui.theme.Theme
import com.mcsnowflake.worktimer.ui.views.STATS_VIEW
import com.mcsnowflake.worktimer.ui.views.StatsView
import com.mcsnowflake.worktimer.ui.views.about.ABOUT_SCREEN
import com.mcsnowflake.worktimer.ui.views.about.AboutScreen
import com.mcsnowflake.worktimer.ui.views.about.SHOW_APP_BAR
import com.mcsnowflake.worktimer.ui.views.credits.CREDITS
import com.mcsnowflake.worktimer.ui.views.credits.Credits
import com.mcsnowflake.worktimer.ui.views.dashboard.DASHBOARD
import com.mcsnowflake.worktimer.ui.views.dashboard.DashBoardViewModel
import com.mcsnowflake.worktimer.ui.views.dashboard.Dashboard
import com.mcsnowflake.worktimer.ui.views.settings.Settings
import com.mcsnowflake.worktimer.ui.views.settings.SettingsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

const val PREFERENCES_SCREEN = "Preferences"
val FIRST_START = booleanPreferencesKey("FIRST_START")
val VERSION = stringPreferencesKey("VERSION")

class MainActivity : ComponentActivity() {

    private val dashBoardViewModel by viewModel<DashBoardViewModel>()
    private val preferences by viewModel<SettingsViewModel> { parametersOf(this) }
    private val Context.dataStore: DataStore<Preferences> by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkForOldSettingsAndConvertToDataStore()

        val permissionRequestLauncher = this@MainActivity.registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) {
            dashBoardViewModel.setReevaluateBanners()
        }

        bindService() // sending the binding request here lets it bind as soon as the service is up

        val startScreen = selectStartScreen()

        setContent {
            Theme {
                // Dialogs
                var showDndDialog by remember { mutableStateOf(false) }
                if (showDndDialog) {
                    DndPermissionDialog(
                        { this@MainActivity.startActivity(it) },
                        { showDndDialog = false }
                    )
                }

                Column(Modifier.fillMaxSize()) {
                    val navController = rememberNavController()
                    NavHost(navController, startScreen) {
                        composable(ABOUT_SCREEN, listOf(navArgument(SHOW_APP_BAR) { type = NavType.BoolType })) {
                            AboutScreen(
                                navController,
                                it.arguments?.getBoolean(SHOW_APP_BAR) ?: false
                            )
                        }
                        composable(CREDITS) { Credits { navController.popBackStack() } }
                        composable(STATS_VIEW) { StatsView { navController.popBackStack() } }
                        composable(DASHBOARD) {
                            Dashboard(
                                navigateTo = { navController.navigate(it) },
                                showDndDialog = { showDndDialog = true },
                                dashBoardViewModel = dashBoardViewModel,
                                permissionRequestLauncher = permissionRequestLauncher
                            )
                        }
                        composable(PREFERENCES_SCREEN) {
                            Settings(
                                showDndDialog = { showDndDialog = true },
                                navigateBack = { navController.popBackStack() },
                                preferences = preferences
                            )
                        }
                    }
                }
            }
        }
    }

    private fun selectStartScreen(): String {
        val firstStart = runBlocking { !dataStore.data.first().contains(FIRST_START) }
        val storedVersion = runBlocking { dataStore.data.first()[VERSION] ?: "" }
        val currentVersion = BuildConfig.VERSION_NAME.substringBefore('-')
        if (storedVersion != currentVersion) lifecycleScope.launch { dataStore.edit { it[VERSION] = currentVersion } }

        return if (firstStart) {
            lifecycleScope.launch { dataStore.edit { it[FIRST_START] = false } }
            ABOUT_SCREEN
        } else {
            DASHBOARD
        }
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        private lateinit var eventsConnector: Job
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as TimerService.LocalBinder
            eventsConnector = lifecycle.coroutineScope.launch {
                binder.getTimerEvents().collect {
                    Log.d("MainActivity", it.toString())
                    dashBoardViewModel.timerState.value = it
                }
            }
            Log.d("MainActivity", "timer service connected")
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            eventsConnector.cancel()
            lifecycle.coroutineScope.launch { dashBoardViewModel.timerState.value = TimerState.NotRunning }
            Log.d("MainActivity", "timer service disconnected")
        }

        override fun onBindingDied(name: ComponentName?) {
            super.onBindingDied(name)
            bindService()
        }
    }

    private fun bindService() {
        bindService(TimerService.getIntent(applicationContext), connection, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(connection)
    }

    private fun checkForOldSettingsAndConvertToDataStore() = lifecycle.coroutineScope.launch(Dispatchers.IO) {
        applicationContext.getSharedPreferences(applicationContext.packageName + "_preferences", MODE_PRIVATE).recoverTo(
            applicationContext.dataStore
        )
    }
}
