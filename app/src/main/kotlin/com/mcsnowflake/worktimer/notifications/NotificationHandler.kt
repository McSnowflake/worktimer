package com.mcsnowflake.worktimer.notifications

import android.app.AlarmManager
import android.app.AlarmManager.ELAPSED_REALTIME_WAKEUP
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.media.AudioAttributes
import android.net.Uri
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.mcsnowflake.worktimer.HYDRATION_MODE
import com.mcsnowflake.worktimer.R
import com.mcsnowflake.worktimer.misc.getOrDefault
import com.mcsnowflake.worktimer.service.TimerState.Running
import com.mcsnowflake.worktimer.service.TimerState.Running.Created
import com.mcsnowflake.worktimer.service.TimerState.Running.Finished
import com.mcsnowflake.worktimer.service.TimerState.Running.Progress
import com.mcsnowflake.worktimer.service.TimerState.Running.Prolonged
import com.mcsnowflake.worktimer.service.TimerState.Running.Started
import com.mcsnowflake.worktimer.service.TimerState.Running.Terminated
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION
import java.time.Duration
import kotlin.math.abs
import kotlinx.coroutines.runBlocking

class NotificationHandler(
    private val applicationContext: Context,
    private val notificationManager: NotificationManager,
    private val alarmManager: AlarmManager,
    private val preferences: DataStore<Preferences>
) {

    private val builder = NotificationBuilder(applicationContext, preferences)

    fun getStartNotification(): Notification = NotificationCompat.Builder(applicationContext, AlarmChannelID).setContentIntent(
        builder.pendingIntent
    ).setSilent(true).setSmallIcon(
        R.drawable.ic_logo_simple
    )
        .setContentTitle("WorkTimer is starting").build()

    fun process(event: Running) {
        when (event) {
            is Created -> {}
            is Started -> {
                alarmManager.setExactAndAllowWhileIdle(ELAPSED_REALTIME_WAKEUP, event.remainingTime.toMillis(), builder.pendingIntent)
            }
            is Prolonged -> {
                alarmManager.cancel(builder.pendingIntent)
                alarmManager.setExactAndAllowWhileIdle(ELAPSED_REALTIME_WAKEUP, event.remainingTime.toMillis(), builder.pendingIntent)
            }
            is Progress -> {
                if (runBlocking { preferences.getOrDefault(HYDRATION_MODE) } && event.isTimeToRemind()) sendHydrationReminder()
                sendProgressUpdate(event)
            }

            is Finished -> {
                sendAlarmNotification(event)
                alarmManager.cancel(builder.pendingIntent)
            }
            is Terminated -> {
                sendStoppedNotification()
                alarmManager.cancel(builder.pendingIntent)
            }
        }
    }

    private fun sendHydrationReminder() = notificationManager.notify(
        HydrationNotificationID,
        builder.buildHydrateNotification()
    )

    private fun sendProgressUpdate(update: Progress) = notificationManager.notify(
        TimerNotificationID,
        builder.buildProgressNotification(update)
    )

    private fun sendAlarmNotification(event: Finished) = notificationManager.notify(
        TimerNotificationID,
        builder.buildAlarmNotification(event)
    )

    private fun sendStoppedNotification() = notificationManager.notify(
        TimerNotificationID,
        builder.buildStoppedNotification()
    )

    private fun Progress.isTimeToRemind() = (
        sessionType == WORK_SESSION && abs(progress - 50) < 40 && remainingTime.toMinutes()
            .rem(15) == 0L
        ) || (sessionType != WORK_SESSION && remainingTime == Duration.ofMinutes(2))

    companion object {
        const val PendingRequestCode = 52334
        const val TimerNotificationID = 2345
        const val HydrationNotificationID = 5346
        const val AlarmChannelID = "47657"
        const val HydrateChannelID = "5253432"

        fun createChannelsWith(applicationContext: Context) {
            NotificationManagerCompat.from(applicationContext).createNotificationChannel(
                NotificationChannel(
                    AlarmChannelID,
                    "WorkTimer Notification Channel",
                    NotificationManager.IMPORTANCE_HIGH
                ).apply {
                    lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                    setShowBadge(false)
                }
            )
            NotificationManagerCompat.from(applicationContext).createNotificationChannel(
                NotificationChannel(
                    HydrateChannelID,
                    "Hydration Reminder Channel",
                    NotificationManager.IMPORTANCE_HIGH
                ).apply {
                    lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                    setShowBadge(false)
                    setSound(
                        Uri.parse(
                            ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + R.raw.drop
                        ),
                        AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).setUsage(
                            AudioAttributes.USAGE_NOTIFICATION_RINGTONE
                        ).build()
                    )
                }
            )
        }
    }
}
