package com.mcsnowflake.worktimer.notifications

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.mcsnowflake.worktimer.MainActivity
import com.mcsnowflake.worktimer.OVERTIME_MODE
import com.mcsnowflake.worktimer.R
import com.mcsnowflake.worktimer.TextHandler.getFormattedTime
import com.mcsnowflake.worktimer.TextHandler.getInfoText
import com.mcsnowflake.worktimer.TextHandler.getTitle
import com.mcsnowflake.worktimer.misc.getOrDefault
import com.mcsnowflake.worktimer.notifications.NotificationHandler.Companion.AlarmChannelID
import com.mcsnowflake.worktimer.notifications.NotificationHandler.Companion.HydrateChannelID
import com.mcsnowflake.worktimer.notifications.NotificationHandler.Companion.PendingRequestCode
import com.mcsnowflake.worktimer.service.TimerAction
import com.mcsnowflake.worktimer.service.TimerService
import com.mcsnowflake.worktimer.service.TimerState.Running
import com.mcsnowflake.worktimer.service.TimerState.Running.Progress
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.LONG_BREAK
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.SHORT_BREAK
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION
import kotlinx.coroutines.runBlocking

class NotificationBuilder(
    private val applicationContext: Context,
    private val preferences: DataStore<Preferences>
) {


    val pendingIntent = PendingIntent.getActivity(
        applicationContext,
        PendingRequestCode,
        Intent(applicationContext, MainActivity::class.java).addCategory(Intent.CATEGORY_LAUNCHER).setAction(
            "android.intent.action.MAIN"
        ),
        PendingIntent.FLAG_IMMUTABLE
    )

    internal fun buildProgressNotification(update: Progress): Notification {
        val builder = NotificationCompat.Builder(applicationContext, AlarmChannelID).setContentIntent(pendingIntent).setSilent(
            true
        ).setSmallIcon(R.drawable.ic_logo_simple)
            .addAction(buildNotificationAction(TimerAction.STOP)).setCategory(Notification.CATEGORY_PROGRESS).setContentTitle(
                getTitle(update)
            )
            .setContentText("${getFormattedTime(update)} remaining").setProgress(
                100,
                update.progress.times(100).toInt(),
                false
            )

        return when (update.sessionType) {
            WORK_SESSION -> builder.addAction(buildNotificationAction(TimerAction.PAUSE))
            SHORT_BREAK, LONG_BREAK -> builder.addAction(buildNotificationAction(TimerAction.WORK))
        }.build()
    }

    internal fun buildAlarmNotification(update: Running): Notification {
        val builder = NotificationCompat.Builder(applicationContext, AlarmChannelID).setContentIntent(pendingIntent).setSmallIcon(
            R.drawable.ic_logo_simple
        )
            .addAction(buildNotificationAction(TimerAction.STOP)).addAction(buildNotificationAction(TimerAction.RESUME)).setAllowSystemGeneratedContextualActions(
                false
            )
            .setCategory(Notification.CATEGORY_ALARM).setContentTitle(getTitle(update)).setContentText(
                getInfoText(update)
            )

        return if (runBlocking {
                preferences.getOrDefault(OVERTIME_MODE)
            }
        ) {
            builder.addAction(buildNotificationAction(TimerAction.MORE)).build()
        } else builder.build()
    }

    internal fun buildStoppedNotification() = NotificationCompat.Builder(applicationContext, AlarmChannelID).setContentIntent(
        pendingIntent
    ).setTimeoutAfter(100).setAutoCancel(true)
        .setSmallIcon(R.drawable.ic_logo_simple).setCategory(Notification.CATEGORY_ALARM).setContentTitle(
            "Timer stopped"
        ).build()

    internal fun buildHydrateNotification() =
        NotificationCompat.Builder(applicationContext, HydrateChannelID).setAutoCancel(true).setTimeoutAfter(10_000).setSmallIcon(
            R.drawable.ic_logo_simple
        )
            .setCategory(Notification.CATEGORY_ALARM).setContentTitle("Remember to hydrate yourself") // TODO refactor out string representations
            .build()

    private fun buildNotificationAction(action: TimerAction): NotificationCompat.Action = NotificationCompat.Action.Builder(
        action.getActionIcon(),
        action.getActionTitle(),
        PendingIntent.getService(
            applicationContext,
            PendingRequestCode,
            TimerService.getIntent(applicationContext, action.serviceCommand),
            PendingIntent.FLAG_IMMUTABLE
        )
    ).build()
}
