package com.mcsnowflake.worktimer.notifications

import android.app.NotificationManager
import android.app.NotificationManager.INTERRUPTION_FILTER_UNKNOWN
import android.util.Log
import com.mcsnowflake.worktimer.service.TimerState.Running
import com.mcsnowflake.worktimer.service.TimerState.Running.Finished
import com.mcsnowflake.worktimer.service.TimerState.Running.Started
import com.mcsnowflake.worktimer.service.TimerState.Running.Terminated
import com.mcsnowflake.worktimer.service.WorkTimer.SessionType.WORK_SESSION

class DoNotDisturbModeHandler(private val notificationManager: NotificationManager) {

    private var previousSetting = INTERRUPTION_FILTER_UNKNOWN

    fun process(state: Running) {
        Log.d("testing", state.toString())
        when {
            state is Started && state.sessionType == WORK_SESSION -> {
                previousSetting = notificationManager.currentInterruptionFilter
                notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_PRIORITY)

                Log.d(javaClass.simpleName, "DND enabled")
            }
            (state is Finished && state.sessionType == WORK_SESSION || state is Terminated) && previousSetting != INTERRUPTION_FILTER_UNKNOWN -> {
                notificationManager.setInterruptionFilter(previousSetting)
                previousSetting = INTERRUPTION_FILTER_UNKNOWN

                Log.d(javaClass.simpleName, "InterruptionFilter reset")
            }
        }
    }

    // TODO check for permission first -> warning toast if no permission + DND mode reset
    //    fun recheckDND() {
//        Log.d(javaClass.simpleName, "recheck dnd")
//        if (preferences.getBoolean("wants_dnd", false) && !internalStates[DND_MODE_KEY]!!.value && notificationManager.isNotificationPolicyAccessGranted) {
//            set(DND_MODE_KEY, true)
//            with(preferences.edit()) {
//                remove("wants_dnd")
//                commit()
//            }
//        }
//    }
}
