package com.mcsnowflake.worktimer

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.mcsnowflake.worktimer.notifications.NotificationHandler
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TimerApplication : Application() {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("worktimer_preferences")

    override fun onCreate() {
        super.onCreate()
        NotificationHandler.createChannelsWith(applicationContext)
        startKoin {
            // androidLogger()
            androidContext(applicationContext)
            modules(getModule(applicationContext.dataStore))
        }
    }
}
