package com.mcsnowflake.worktimer

import android.content.SharedPreferences
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey

val WORK_SESSION_DURATION = intPreferencesKey("work_session_duration")
val SHORT_BREAK_DURATION = intPreferencesKey("short_break_duration")
val LONG_BREAK_DURATION = intPreferencesKey("long_break_duration")
val SHORT_BREAK_REPETITIONS = intPreferencesKey("short_break_repetitions")
val OVERTIME_DURATION = intPreferencesKey("overtime_duration")

val OVERTIME_MODE = booleanPreferencesKey("overtime_mode")
val DND_MODE = booleanPreferencesKey("dnd_mode")
val HYDRATION_MODE = booleanPreferencesKey("hydrate_mode")
val AUTO_SWITCH_MODE = booleanPreferencesKey("auto_switch_mode")

val STATISTICS_ON = booleanPreferencesKey("statistics_on")

val TIMER_INPUTS: Map<Preferences.Key<Int>, Int> = mapOf(
    // durations in minutes
    Pair(WORK_SESSION_DURATION, 25),
    Pair(SHORT_BREAK_REPETITIONS, 3),
    Pair(SHORT_BREAK_DURATION, 5),
    Pair(LONG_BREAK_DURATION, 10),
    Pair(OVERTIME_DURATION, 5)
)

val PREFERENCES: Map<Preferences.Key<Boolean>, Boolean> = mapOf(
    Pair(STATISTICS_ON, false),
    Pair(OVERTIME_MODE, false),
    Pair(DND_MODE, false),
    Pair(HYDRATION_MODE, false),
    Pair(AUTO_SWITCH_MODE, false)
)

val SETTINGS_AND_DEFAULTS: Map<Preferences.Key<*>, Any> = TIMER_INPUTS + PREFERENCES

@Suppress("UNCHECKED_CAST")
fun <T> Preferences.Key<T>.getDefault() = SETTINGS_AND_DEFAULTS[this]!! as T

suspend fun SharedPreferences.recoverTo(store: DataStore<Preferences>) {
    if (this.all.isNotEmpty()) store.edit {

        if (this.contains(STATISTICS_ON.name))
            it[STATISTICS_ON] = getBoolean(STATISTICS_ON.name, STATISTICS_ON.getDefault())

        // mode settings
        if (this.contains(DND_MODE.name))
            it[DND_MODE] = getBoolean(DND_MODE.name, DND_MODE.getDefault())

        if (this.contains(HYDRATION_MODE.name))
            it[HYDRATION_MODE] = getBoolean(HYDRATION_MODE.name, HYDRATION_MODE.getDefault())

        if (this.contains(AUTO_SWITCH_MODE.name))
            it[AUTO_SWITCH_MODE] = getBoolean(AUTO_SWITCH_MODE.name, AUTO_SWITCH_MODE.getDefault())

        if (this.contains(OVERTIME_MODE.name))
            it[OVERTIME_MODE] = getBoolean(OVERTIME_MODE.name, OVERTIME_MODE.getDefault())

        // timer settings
        if (this.contains(WORK_SESSION_DURATION.name))
            it[WORK_SESSION_DURATION] = getInt(WORK_SESSION_DURATION.name, WORK_SESSION_DURATION.getDefault())

        if (this.contains(SHORT_BREAK_DURATION.name))
            it[SHORT_BREAK_DURATION] = getInt(SHORT_BREAK_DURATION.name, SHORT_BREAK_DURATION.getDefault())

        if (this.contains(LONG_BREAK_DURATION.name))
            it[LONG_BREAK_DURATION] = getInt(LONG_BREAK_DURATION.name, LONG_BREAK_DURATION.getDefault())

        if (this.contains(SHORT_BREAK_REPETITIONS.name))
            it[SHORT_BREAK_REPETITIONS] = getInt(SHORT_BREAK_REPETITIONS.name, SHORT_BREAK_REPETITIONS.getDefault())
    }
}
